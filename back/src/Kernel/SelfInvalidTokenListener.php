<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 14/01/19
 * Time: 00:45
 */
namespace App\Kernel;

use App\Exception\SelfInvalidTokenException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class SelfInvalidTokenListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof SelfInvalidTokenException)
        {
            $event->setResponse($event->getException()->getRedirectResponse());
        }
    }
}