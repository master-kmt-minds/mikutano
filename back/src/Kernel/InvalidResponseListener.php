<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 14/01/19
 * Time: 19:18
 */
namespace App\Kernel;

use App\Exception\InvalidResponseException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class InvalidResponseListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof InvalidResponseException)
        {
            $event->setResponse($event->getException()->getRedirectResponse());
        }
    }

}