<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 12/12/18
 * Time: 23:43
 */
class OverviewController extends Controller
{
    /**
     * @Route("/admin", name="home")
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }
}