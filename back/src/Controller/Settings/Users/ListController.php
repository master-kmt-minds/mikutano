<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Users;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/admin/settings/users/list", name="users_list")
     */
    public function index(UserService $userService)
    {
        return $this->render('settings/users/list.html.twig', [
            'list' => $userService->getList(),
        ]);
    }
}