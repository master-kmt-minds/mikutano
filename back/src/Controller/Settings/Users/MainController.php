<?php

namespace App\Controller\Settings\Users;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:24
 */

class MainController extends Controller
{
    /**
     * @Route("/admin/settings/users", name="users")
     */
    public function index()
    {
        return $this->render('settings/users/main.html.twig');
    }
}