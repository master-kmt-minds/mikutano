<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/01/19
 * Time: 14:24
 */

namespace App\Controller\Settings\Users;


use App\Exception\InvalidResponseException;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class PasswordController extends Controller
{
    /**
     * @Route("/admin/settings/users/password", name="user_password")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, UserService $userService)
    {
        $defaultData = ['message' => 'Password form'];
        $form = $this->createFormBuilder($defaultData)
            ->add('current_password', PasswordType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'options' => [
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $userService->changePassword($request->request->get('form')['current_password'], $request->request->get('form')['password']['first']);
                return new JsonResponse();
            } else{
                throw new InvalidResponseException(null, 'Bad request', 500);
            }
        } else{
            return $this->render('settings/users/password.html.twig', [
                'oForm' => $form->createView(),
            ]);
        }

    }
}