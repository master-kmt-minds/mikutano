<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Users;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SaveController extends Controller
{
    /**
     * @param Request $request
     * @param UserService $userService
     * @param null $id
     *
     * @Route("/admin/users/save/{id}", name="users_save")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, UserService $userService, $id = null)
    {
        if ($request->isXmlHttpRequest() && !empty($request->get('user'))) {
            $data = $request->get('user');
            if (!isset($data['active']))
                $data['active'] = false;

            if (!isset($data['blocked']))
                $data['blocked'] = false;

            if ($id === null) {
                $userService->createItem($data);
            } else {
                $userService->updateItem($data, $id);
            }

            return $this->render('settings/users/list.html.twig', [
                'list' => $userService->getList(),
            ]);
        }
    }
}