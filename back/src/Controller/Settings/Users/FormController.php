<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Users;


use App\Entity\User;
use App\Form\UserType;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/admin/settings/users/form/{id}", name="users_form")
     * @param UserService $userService
     * @param null $id
     * @return Response
     */
    public function index(UserService $userService, $id = null)
    {
        if ($id === null) {
            $form = $userService->getForm(UserType::class);
        }
        else {
            $form = $userService->getForm(UserType::class, new User(null, null, null), $id);
        }

        return $this->render('settings/users/form.html.twig', [
            'oForm' => $form,
            'id' => $id,
        ]);
    }
}