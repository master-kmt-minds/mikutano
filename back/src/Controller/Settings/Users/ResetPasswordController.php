<?php


namespace App\Controller\Settings\Users;


use App\Exception\InvalidResponseException;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends Controller
{
    /**
     * @Route("/recover_password", name="users_recover_password")
     */
    public function recover(Request $request, UserService $userService)
    {
        $email = !empty($request->request->get('email')) ? $request->request->get('email') : '';

        if (empty($email)) {
            return new JsonResponse('Provide a valid email', 400);
        }

        $userService->recoverPassword($email);

        return new JsonResponse('Ok', 200);
    }

    /**
     * @Route("/reset_password", name="reset_password")
     */
    public function index(Request $request)
    {
        $email = !empty($request->query->get('email')) ? $request->query->get('email') : 'xx';
        $token = !empty($request->query->get('token')) ? $request->query->get('token') : 'xx';

        return $this->render('settings/users/reset_password.html.twig', [
            'email' => $email,
            'token' => $token,
        ]);
    }

    /**
     * @Route("/resetpassword", name="users_reset_password")
     */
    public function resetPassword(Request $request, UserService $userService)
    {
        $email = !empty($request->request->get('email')) ? $request->request->get('email') : 'xx';
        $token = !empty($request->request->get('token')) ? $request->request->get('token') : 'xx';
        $pwd1 = !empty($request->request->get('password_first')) ? $request->request->get('password_first') : '';
        $pwd2 = !empty($request->request->get('password_second')) ? $request->request->get('password_second') : '';

        if ($pwd1 !== $pwd2) {
            return new JsonResponse('Password are different', 400);
        }

        $userService->resetPassword($email, $token, $pwd1);
        return new JsonResponse('OK', 200);

    }
}