<?php


namespace App\Controller\Settings\Users;


use App\Entity\User;
use App\Exception\InvalidResponseException;
use App\Form\UserType;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ConfirmRegistrationController extends Controller
{
    /**
     * @Route("/confirm", name="users_confirm")
     */
    public function index(Request $request, UserService $userService)
    {
        $email = !empty($request->query->get('email')) ? $request->query->get('email') : '';
        $token = !empty($request->query->get('token')) ? $request->query->get('token') : '';

        try {
            $userService->confirmRegistration($email, $token);
            return $this->render('settings/users/confirm.html.twig');
        } catch (InvalidResponseException $exception) {
            return $this->render('settings/users/confirm.html.twig', [
                'message' => $exception->getRedirectResponse()->getContent()
            ]);
        }
    }
}