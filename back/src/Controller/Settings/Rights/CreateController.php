<?php


namespace App\Controller\Settings\Rights;


use App\Service\BORightService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends Controller
{
    /**
     * @Route("/admin/settings/rights/new", name="rights_new")
     */
    public function index(Request $request, BORightService $rightService)
    {
        $rightService->createProfile($request->request->all());
        return new JsonResponse();
    }
}