<?php

namespace App\Controller\Settings\Rights;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    /**
     * @Route("/admin/settings/rights", name="rights")
     */
    public function index()
    {
        return $this->render('settings/rights/main.html.twig');
    }
}