<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Rights;


use App\Service\BORightService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/admin/settings/rights/form", name="rights_form")
     */
    public function index()
    {
        return $this->render('settings/rights/form.html.twig');
    }
}