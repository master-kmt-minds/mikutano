<?php


namespace App\Controller\Settings\Rights;


use App\Service\BORightService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SaveController extends Controller
{
    /**
     * @Route("/admin/settings/rights/update", name="rights_update")
     */
    public function index(BORightService $rightService, Request $request)
    {
        $rightService->updateRight($request->request->all());
        return new JsonResponse();
    }
}