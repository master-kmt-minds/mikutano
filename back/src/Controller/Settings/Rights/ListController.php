<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Rights;

use App\Service\BORightService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/admin/settings/rights/list", name="rights_list")
     */
    public function index(BORightService $rightService)
    {
        //var_dump($rightService->getListProfiles());die;
        return $this->render('settings/rights/list.html.twig', [
            'rights' => (array)$rightService->getList(),
            'securityZones' => $rightService->getSecurityZones(),
            'profiles' => $rightService->getListProfiles()
        ]);
    }
}