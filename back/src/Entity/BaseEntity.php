<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 17/01/19
 * Time: 17:09
 */

namespace App\Entity;


abstract class BaseEntity
{
    /**
     * @param $arrayValues
     */
    public function hydrateMe($arrayValues)
    {
        foreach ($arrayValues as $key=>$value)
        {
            if (is_numeric($value) && strlen($value) == 1 && $key !== "id")
            {
                $fct = "set".ucfirst($key);
                if (method_exists($this, $fct))
                {
                    $this->$fct($this->getBooleanValue($value));
                }
            }else{
                $fct = "set".ucfirst($key);
                if (method_exists($this, $fct))
                {
                    $this->$fct($value);
                }
            }
        }
    }

    public function getBooleanValue( $intVal)
    {
        if ($intVal === '1' || $intVal === 1 )
        {
            return true;
        }else{
            return false;
        }
    }
}