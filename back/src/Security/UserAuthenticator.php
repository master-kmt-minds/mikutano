<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 15/01/19
 * Time: 01:01
 */

namespace App\Security;


use App\Entity\User;
use App\Exception\InvalidResponseException;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;

class UserAuthenticator implements SimpleFormAuthenticatorInterface
{
    private $userService;

    /**
     * UserAuthenticator constructor.
     * @param $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        try {
            $userp = $userProvider->loadUserByUsername($token->getUsername());
        } catch (UsernameNotFoundException $exception) {
            throw new AuthenticationException('Invalid username or password');
        }
        $query = array(
            'email' => $token->getUsername(),
            'password' => $token->getCredentials(),
        );
        $headers = array('Content-Type' => 'application/ld+json');

        $response = \Unirest\Request::post(getenv('API_LOGIN_CHECK'), $headers, json_encode($query));
        if ($response->code === 200 )
        {
            $session = new Session();
            $session->set('token', $response->body->token);
            $session->set('refresh_token', $response->body->refresh_token);

            $rights = $this->userService->getUserRoles($response->body->profile);

            if ($rights === null) {
                throw new CustomUserMessageAuthenticationException("Impossible");
            }
            $user = new User($userp->getUsername(),null, $rights);
            return new UsernamePasswordToken(
                $user,
                $user->getPassword(),
                $providerKey,
                $user->getRoles()
            );
        }else{
            $errorMessage = (new InvalidResponseException($response))->getRedirectResponse();
            throw new CustomUserMessageAuthenticationException($errorMessage->getContent());
        }
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken
            && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }
}