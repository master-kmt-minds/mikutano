<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 04/01/19
 * Time: 00:08
 */

namespace App\Service;

use App\Exception\InvalidResponseException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Unirest\Method;

abstract class BOBaseService
{
    protected $routeName;

    /**
     * @var array $fieldsName
     * [apiName => fieldName]
     */
    protected $fieldsName = [];
    protected $helper;
    protected $formFactory;

    /**
     * UserService constructor.
     * @param BOHelperService $helperService
     */
    public function __construct(FormFactoryInterface $factory, BOHelperService $helperService)
    {
        $this->formFactory = $factory;
        $this->helper = $helperService;
    }


    public function getItem($key)
    {
        $response = $this->helper->requestApi(Method::GET,getenv('API_URL') .'/'.$this->routeName.'/'.$key, null);

        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body->data;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function getList($page = 1)
    {
        $response = $this->helper->requestApi(Method::GET, getenv('API_URL') .'/'.$this->routeName.'?page='.$page, null);
        if ($response->code === ResponseCode::HTTP_OK) {
            return !empty($response->body->{'hydra:member'}) ? $response->body->{'hydra:member'} : $response->body->data;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function getSettingsByType($typename)
    {
        $response = $this->helper->requestApi(Method::GET, getenv('API_URL') .'/settings/'.$typename, null);
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body->data;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function deleteItem($key)
    {
        $response = $this->helper->requestApi(Method::DELETE, getenv('API_URL') .'/'.$this->routeName.'/'.$key, null);

        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function createItem($data)
    {
        $response = $this->helper->requestApi(Method::POST,getenv('API_URL') .'/'.$this->routeName, $this->helper->makeJsonBody($data, $this->fieldsName, true));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function updateItem($data, $key)
    {
        $response = $this->helper->requestApi(Method::PUT,getenv('API_URL') .'/'.$this->routeName.'/'.$key, $this->helper->makeJsonBody($data, $this->fieldsName, false));

        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function getForm($type, $entity = null, $key = null)
    {
        if ($key === null) {
            return $this->formFactory->createBuilder($type)->getForm()->createView();
        }

        $response = $this->helper->requestApi(Method::GET,getenv('API_URL') .'/'.$this->routeName.'/'.$key, null);
        if ($response->code === ResponseCode::HTTP_OK) {
            $entity->hydrateMe((array)$response->body->data);
            return $this->formFactory->createBuilder($type, $entity)->getForm()->createView();
        } else {
            return $this->formFactory->createBuilder($type)->getForm()->createView();
        }
    }
}