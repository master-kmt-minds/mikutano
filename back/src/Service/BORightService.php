<?php


namespace App\Service;


use App\Exception\InvalidResponseException;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Unirest\Method;
use Unirest\Request;

class BORightService extends BOBaseService
{
    protected $routeName = "rights";

    public function getSecurityZones()
    {
        $response = $this->helper->requestApi(Method::GET, getenv('API_URL') .'/settings/securityZone', null);
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body->data;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function getListProfiles()
    {
        $response = $this->helper->requestApi(Method::GET, getenv('API_URL') .'/profiles', null);
        if ($response->code === ResponseCode::HTTP_OK) {
            $data = $response->body->data;
            $nameProfiles=[];
            foreach ($data as $profile) {
                $nameProfiles[$profile->{'profile'}] = $profile->{'nom'};
            }
            return $nameProfiles;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function createProfile($data)
    {
        $body=[];
        $body['nom'] = $data['nom'];
        $body['profile'] = $data['profile'];
        $response = $this->helper->requestApi(Method::POST, getenv('API_URL') .'/profiles', Request\Body::Json($body));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function updateRight($right)
    {
        $body['profile'] = $right['profile'];
        $body['zone'] = $right['zone'];
        $body['level'] = (int)$right['level'];

        $response = $this->helper->requestApi(Method::PUT, getenv('API_URL') .'/rights', Request\Body::Json($body));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response;
        } else{
            throw new InvalidResponseException($response);
        }
    }

}