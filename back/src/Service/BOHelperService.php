<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 12/01/19
 * Time: 02:38
 */

namespace App\Service;

use App\Exception\InvalidResponseException;
use App\Exception\SelfInvalidTokenException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Unirest\Exception;
use Unirest\Method;
use Unirest\Request;

class BOHelperService
{
    /**
     * HelperService constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param $method
     * @param $url
     * @param $body
     * @return \Unirest\Response|void
     * @throws \App\Exception\SelfInvalidTokenException|\Unirest\Exception
     */
    function requestApi($method, $url, $body = null)
    {
        $response = Request::send($method, $url, $body, self::getHeader());
        if ($response->code === 200) {
            return $response;
        } elseif ($response->code === 401) {//Token expired
            if (self::refreshJWTToken()) {
                return Request::send($method, $url, $body, self::getHeader());
            } else {
                self::logout();
            }
        } else {
            return $response;
        }
    }

    private function getHeader()
    {
        $session = new Session();
        $token = $session->get('token');

        if (!empty($token)) {
            return array('Authorization' => 'Bearer ' . $token, 'Accept' => 'application/ld+json', 'Content-type' => 'application/json');
        } else {
            self::logout();
        }

    }

    protected function refreshJWTToken()
    {
        $session = new Session();
        $refreshToken = $session->get('refresh_token');
        if (!empty($refreshToken)) {
            $query = array('refresh_token' => $refreshToken,);
            $headers = array('Content-Type' => 'application/json');
            $response = Request::post(getenv('API_REFRESH_TOKEN'), $headers, Request\Body::Json($query));
            if ($response->code === 200) {
                $session->set('token', json_decode($response->raw_body)->token);
                $session->set('refresh_token', json_decode($response->raw_body)->refresh_token);
                return true;
            } else {
                return false;
            }
        } else {
            self::logout();
        }
    }

    /**
     * connect to the api with a guest account to perform some action (confirm registration, reset password)
     */
    public function fakeLoginCheck()
    {
        $query = array(
            'email' => 'guest@sdk.com',
            'password' => 'Pass@1234',
        );
        $headers = array('Content-Type' => 'application/ld+json');

        $response = \Unirest\Request::post(getenv('API_LOGIN_CHECK'), $headers, json_encode($query));
        if ($response->code === 200 ) {
            $session = new Session();
            $session->set('token', $response->body->token);
            $session->set('refresh_token', $response->body->refresh_token);
        }else{
            $errorMessage = (new InvalidResponseException($response))->getRedirectResponse();
            throw new CustomUserMessageAuthenticationException($errorMessage->getContent());
        }
    }

    /**
     * @param array $formData
     * @param array $fieldsName
     * @param bool $setEmpty
     * @return false|string
     * @throws Exception
     */
    public function makeJsonBody(array $formData, array $fieldsName, $setEmpty = false)
    {
        $JSONBody = array();

        foreach ($fieldsName as $name) {

            if (key_exists($name, $formData)) {
                // Rajout du isset pour ne pas transmettre un null si le champ est vide
                if (isset($formData[$name])) {
                    if (isset($formData[$name]->key)) {
                        $JSONBody[$name] = $formData[$name]->key;
                    } else {
                        $JSONBody[$name] = $formData[$name];
                    }
                }
            } // Set empty field / Case Create POST
            elseif ($setEmpty) {
                $JSONBody[$name] = null;
            }
            // Not set empty field / Case Update PUT
        }
        return Request\Body::Json($JSONBody);
    }

    public function logout()
    {
        throw new SelfInvalidTokenException(new RedirectResponse($this->router->generate('logout')));
    }

    public function getAllProfiles()
    {
        $response = $this->requestApi(Method::GET, getenv('API_URL') .'/profiles', null);
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body->data ;
        } else{
            throw new InvalidResponseException($response);
        }
    }

}