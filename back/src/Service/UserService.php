<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 11/01/19
 * Time: 16:41
 */

namespace App\Service;

use App\Exception\InvalidResponseException;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Unirest\Method;
use Unirest\Request\Body;

class UserService extends BOBaseService
{
    protected $routeName = "users";
    protected $fieldsName = [
        'nom' => 'nom',
        'prenom' => 'prenom',
        'ville' => 'ville',
        'telephone' => 'telephone',
        'email' => 'email',
        'profile' => 'profile',
        'active' => 'active',
        'blocked' => 'blocked',
    ];

    public function changePassword($current, $new){
        $response = $this->helper->requestApi(Method::POST,getenv('API_URL') .'/users/me/changepassword?current_password='.$current, Body::Json(['password' => $new]));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function getUserRoles($profile)
    {
        $response = $this->helper->requestApi(Method::GET, getenv('API_URL') . '/rights/' . $profile);
        if ($response->code === ResponseCode::HTTP_OK) {
            $roles = [];
            array_push($roles, 'ROLE_ADMIN');
            $rights = $response->body->data;

            foreach ($rights as $right) {
                if ($right->level === 1) {
                    array_push($roles, 'ROLE_'.$right->zone.'_RO');
                }

                if ($right->level === 2) {
                    array_push($roles, 'ROLE_'.$right->zone.'_RW');
                }
            }

            return $roles;
        } else {
            return null;
        }
    }

    public function confirmRegistration(string $email, string $token)
    {
        $this->helper->fakeLoginCheck();
        $response = $this->helper->requestApi(Method::PUT,getenv('API_URL') .'/users/confirm/'.$token, Body::Json(['email' => $email]));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function recoverPassword(string $email)
    {
        $this->helper->fakeLoginCheck();
        $response = $this->helper->requestApi(Method::PUT,getenv('API_URL') .'/users/me/recover', Body::Json([
            'email' => $email
        ]));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }

    public function resetPassword(string $email, string $token, string $password)
    {
        $this->helper->fakeLoginCheck();
        $response = $this->helper->requestApi(Method::PUT,getenv('API_URL') .'/users/me/resetpassword/'.$token, Body::Json([
            'email' => $email,
            'password' => $password
        ]));
        if ($response->code === ResponseCode::HTTP_OK) {
            return $response->body;
        } else{
            throw new InvalidResponseException($response);
        }
    }
}