<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 11/01/19
 * Time: 16:33
 */

namespace App\Form;

use App\Entity\User;
use App\Service\BOHelperService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    private $helper;

    /**
     * UserType constructor.
     */
    public function __construct(BOHelperService $helperService)
    {
        $this->helper = $helperService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('prenom', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('ville', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('telephone', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'data-smk-type' => 'number',
                    'minlength' => 9,
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('profile', ChoiceType::class, [
                'choices' => $this->helper->getAllProfiles(),
                'choice_label' => 'nom',
                'choice_value' => 'profile',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('active', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('blocked', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}