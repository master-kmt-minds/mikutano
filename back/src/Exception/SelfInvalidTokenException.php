<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 14/01/19
 * Time: 00:43
 */

namespace App\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SelfInvalidTokenException extends Exception
{
    private $redirectResponse;

    public function __construct(RedirectResponse $redirectResponse, $message = '', $code = 0,\Exception $previousException = null)
    {
        $this->redirectResponse = $redirectResponse;
        parent::__construct($message, $code, $previousException);
    }

    public function getRedirectResponse()
    {
        return $this->redirectResponse;
    }

}