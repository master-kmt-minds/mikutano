<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 14/01/19
 * Time: 19:18
 */

namespace App\Exception;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class InvalidResponseException extends Exception
{
    protected $restResponse;
    protected $message;
    protected $code;

    public function __construct($restResponse, $message = '', $code = 0,\Exception $previousException = null)
    {
        $this->restResponse = $restResponse;
        $this->message = $message;
        $this->code = $code;
        parent::__construct($message, $code, $previousException);
    }


    public function getRestResponse()
    {
        return $this->restResponse;
    }

    public function getRedirectResponse()
    {
        if ($this->message !== '')
        {
            return new JsonResponse($this->message, $this->code);
        }
        elseif (isset($this->restResponse->body->{'error'})) //error badd request (missed key, for example)
        {
            $message = $this->restResponse->body->{'error'}->{'message'};
        }
        elseif (isset($this->restResponse->body->{'message'})) //error handled by the API dev
        {
            $message = $this->restResponse->body->{'message'};
        } elseif (isset($this->restResponse->body->{'hydra:description'})) //error not handled by the API dev
        {
            $message = $this->restResponse->body->{'hydra:description'};
        } elseif (isset($this->restResponse->body->{'detail'})) //error connection to db
        {
            $message = $this->restResponse->body->{'detail'};
        } else{
            $message = 'Not Found route';
        }
        return new JsonResponse($message, $this->restResponse->code);
    }
}