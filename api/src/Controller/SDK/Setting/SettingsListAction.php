<?php


namespace App\Controller\SDK\Setting;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Service\SDK\SettingService;

class SettingsListAction extends BaseAction
{
    public function __invoke(SettingService $settingService, string $typename)
    {
        $this->apiResponse = $settingService->listSettingsByType($this->apiResponse, $typename);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}