<?php


namespace App\Controller\SDK\Right;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\RightDto;
use App\Service\SDK\RightService;

class RightCreateAction extends BaseAction
{
    public function __invoke(RightService $rightService, RightDto $data)
    {
        $this->apiResponse = $rightService->create($this->apiResponse, $data);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}