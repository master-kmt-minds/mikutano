<?php


namespace App\Controller\SDK\Right;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Service\SDK\ProfileService;
use App\Service\SDK\RightService;

class RightsListAction extends BaseAction
{
    public function __invoke(RightService $rightService, ProfileService $profileService)
    {
        $this->apiResponse = $rightService->listRights($this->apiResponse, $profileService);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}