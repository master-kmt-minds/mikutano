<?php


namespace App\Controller\SDK\Right;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Service\SDK\RightService;

class RightsProfileAction extends BaseAction
{
    public function __invoke(RightService $rightService, string $profile)
    {
        $this->apiResponse = $rightService->listRightsByProfile($this->apiResponse, $profile);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}