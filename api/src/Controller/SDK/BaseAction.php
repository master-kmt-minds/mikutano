<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 20/03/19
 * Time: 12:55
 */

namespace App\Controller\SDK;

use App\Entity\SDK\ApiResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseAction
{
    abstract public function getSecurityZone(): string;

    protected $apiResponse;

    /**
     * BaseAction constructor.
     */
    public function __construct()
    {
        $this->apiResponse = new ApiResponse();
    }

    protected function returnApiResponse()
    {
        if ($this->apiResponse->getCode() == 1) {
            return new JsonResponse(
                array(
                    "code" => $this->apiResponse->getCode(),
                    "message" => $this->apiResponse->getMessage(),
                    "context" => $this->apiResponse->getContext(),
                    "data" => $this->apiResponse->getData()),
                200);
        }
        else {
            return new JsonResponse(
                array (
                    "code" => $this->apiResponse->getCode(),
                    "message" => $this->apiResponse->getMessage()),
                400);
        }
    }

}