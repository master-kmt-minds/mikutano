<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/01/19
 * Time: 08:07
 */

namespace App\Controller\SDK\User;
use App\Constants;
use App\Controller\SDK\BaseAction;
use App\DTO\SDK\UserDto;
use App\Service\SDK\UserService;
use Symfony\Component\Security\Core\Security;

class UserRecoverPasswordAction extends BaseAction
{

    public function __invoke(UserService $userService, UserDto $data)
    {
        $this->apiResponse = $userService->recover($this->apiResponse, $data);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_OPEN_ZONE;
    }
}