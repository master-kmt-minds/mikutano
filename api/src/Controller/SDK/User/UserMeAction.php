<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/01/19
 * Time: 08:07
 */

namespace App\Controller\SDK\User;
use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\SDK\User;
use Symfony\Component\Security\Core\Security;

class UserMeAction extends BaseAction
{

    public function __invoke(Security $security)
    {
        return $security->getUser();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}