<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/03/19
 * Time: 13:22
 */

namespace App\Controller\SDK\User;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\UserDto;
use App\Entity\SDK\User;
use App\Service\SDK\UserService;

class UserCreateAction extends BaseAction
{
    public function __invoke(UserService $userService, UserDto $data)
    {
        $this->apiResponse = $userService->create($this->apiResponse, $data);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}