<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/01/19
 * Time: 08:07
 */

namespace App\Controller\SDK\User;
use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\SDK\User;
use App\Service\SDK\UserService;

class UserListAction extends BaseAction
{
    public function __invoke(UserService $userService)
    {
        $this->apiResponse = $userService->listAll($this->apiResponse);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}