<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/01/19
 * Time: 08:07
 */

namespace App\Controller\SDK\User;
use App\Constants;
use App\Controller\SDK\BaseAction;
use App\DTO\SDK\UserDto;
use App\Service\SDK\UserService;

class UserUpdateAction extends BaseAction
{
    public function __invoke(UserService $userService, int $id, UserDto $data)
    {
        $this->apiResponse = $userService->update($this->apiResponse, $data, $id);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}