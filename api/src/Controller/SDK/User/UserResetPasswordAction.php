<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 07/02/19
 * Time: 17:08
 */

namespace App\Controller\SDK\User;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\UserDto;
use App\Service\SDK\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserResetPasswordAction extends BaseAction
{
    public function __invoke(Request $request, UserService $userService, UserDto $data, string $token)
    {
        $this->apiResponse = $userService->resetPassword($this->apiResponse, $data, $token);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_OPEN_ZONE;
    }
}