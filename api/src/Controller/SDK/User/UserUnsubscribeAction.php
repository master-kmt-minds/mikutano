<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 24/01/19
 * Time: 08:07
 */

namespace App\Controller\SDK\User;
use App\Constants;
use App\Controller\SDK\BaseAction;
use App\DTO\SDK\UserDto;
use App\Service\SDK\UserService;
use Symfony\Component\Security\Core\Security;

class UserUnsubscribeAction extends BaseAction
{

    public function __invoke(UserService $userService, Security $security)
    {
        $id = !is_null($security->getUser()->getId()) ? $security->getUser()->getId() : 0;
        $this->apiResponse = $userService->unsubscribe($this->apiResponse, $id);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_USERS_ZONE;
    }
}