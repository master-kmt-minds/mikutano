<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 07/02/19
 * Time: 17:08
 */

namespace App\Controller\SDK\User;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\UserDto;
use App\Service\SDK\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserChangePasswordAction extends BaseAction
{
    public function __invoke(Request $request, UserService $userService, UserDto $data, Security $security)
    {
        $prp = !empty($request->query->get('current_password')) ? $request->query->get('current_password') : '';
        $currentUid = !is_null($security->getUser()->getId()) ? $security->getUser()->getId() : 0;
        $this->apiResponse = $userService->changePassword($this->apiResponse, $data, $currentUid, $prp);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}