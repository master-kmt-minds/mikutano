<?php


namespace App\Controller\SDK\Profile;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\ProfileDto;
use App\Service\SDK\ProfileService;
use App\Service\SDK\UserService;

class ProfileCreateAction extends BaseAction
{
    public function __invoke(ProfileService $profileService, ProfileDto $data)
    {
        $this->apiResponse = $profileService->create($this->apiResponse, $data);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}