<?php


namespace App\Controller\SDK\Profile;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Dto\SDK\ProfileDto;
use App\Service\SDK\ProfileService;
use App\Service\SDK\UserService;

class ProfileUpdateAction extends BaseAction
{
    public function __invoke(ProfileService $profileService, ProfileDto $data, int $id)
    {
        $this->apiResponse = $profileService->update($this->apiResponse, $data, $id);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}