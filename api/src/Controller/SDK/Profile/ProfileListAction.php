<?php


namespace App\Controller\SDK\Profile;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Service\SDK\ProfileService;
use App\Service\SDK\UserService;

class ProfileListAction extends BaseAction
{
    public function __invoke(ProfileService $profileService)
    {
        $this->apiResponse = $profileService->list($this->apiResponse);
        return $this->returnApiResponse();
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}