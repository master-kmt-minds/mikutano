<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;
use App\Service\EntityService;

class ConferenceDeleteAction extends BaseAction
{
    public function __invoke(EntityService $entityService, Conference $data)
    {
        $entityService->markAsDeleted($data, Conference::class);
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}