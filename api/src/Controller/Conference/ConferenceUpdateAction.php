<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;

class ConferenceUpdateAction extends BaseAction
{
    public function __invoke(Conference $data)
    {
        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}