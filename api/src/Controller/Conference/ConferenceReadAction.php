<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConferenceReadAction extends BaseAction
{
    public function __invoke(Conference $data)
    {
        if ($data->getDeleted())
            throw new NotFoundHttpException();

        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}