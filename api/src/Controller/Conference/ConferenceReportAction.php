<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class ConferenceReportAction extends BaseAction
{
    private $container;
    private $twig;
    /**
     * ConferenceReportAction constructor.
     */
    public function __construct(ContainerInterface $container, Environment $twig)
    {
        parent::__construct();
        $this->container = $container;
        $this->twig = $twig;
    }

    public function __invoke(Conference $data)
    {
        $options = new Options();
        $options->set('defaultFont', 'Ubuntu');

        $dompdf = new Dompdf($options);

        $html = $this->twig->render(
            'export/event.html.twig',
            [
                'title' => $data->getTitle(),
                'address' => $data->getAddress(),
                'startDate' => $data->getDateStart().' - '. $data->getHourStart(),
                'endDate' => $data->getDateEnd().' - '. $data->getHourEnd(),
                'paf' => $data->getPrice(),
                'nbSpeaker' => count($data->getSpeakers()),
                'nbAttendee' => count($data->getParticipants()),
                'speakers' => $data->getSpeakers(),
                'attendees' => $data->getParticipants(),
            ]
        );

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("{$data->getTitle()}.pdf", [
            "Attachment" => true
        ]);
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}