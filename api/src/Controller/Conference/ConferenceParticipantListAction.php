<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;
use App\Entity\Participant;
use Symfony\Component\Routing\Annotation\Route;

class ConferenceParticipantListAction extends BaseAction
{
    public function __invoke($data)
    {
        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}