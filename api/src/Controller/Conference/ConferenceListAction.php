<?php

namespace App\Controller\Conference;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Conference;
use Symfony\Component\HttpFoundation\Request;

class ConferenceListAction extends BaseAction
{
    public function __invoke(Request $request, $data)
    {
        $datas = [];
        /**
         * @var Conference $conf
         */
        foreach ($data as $conf) {
            if (!$conf->getDeleted()) {
                $datas[] = $conf;
            }
        }

        return $datas;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}