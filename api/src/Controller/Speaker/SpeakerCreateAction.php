<?php

namespace App\Controller\Speaker;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Speaker;

class SpeakerCreateAction extends BaseAction
{
    public function __invoke(Speaker $data)
    {
        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}