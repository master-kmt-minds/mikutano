<?php

namespace App\Controller\Speaker;

use App\Constants;
use App\Controller\SDK\BaseAction;
use Symfony\Component\HttpFoundation\Request;

class SpeakerListAction extends BaseAction
{
    public function __invoke(Request $request, $data)
    {
        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}