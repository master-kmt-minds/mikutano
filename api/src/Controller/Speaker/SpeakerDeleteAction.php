<?php

namespace App\Controller\Speaker;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Speaker;
use App\Service\EntityService;

class SpeakerDeleteAction extends BaseAction
{
    public function __invoke(EntityService $entityService, Speaker $data)
    {
        $entityService->markAsDeleted($data, Speaker::class);
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}