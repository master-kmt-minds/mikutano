<?php

namespace App\Controller\Participant;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Participant;
use App\Service\EntityService;

class ParticipantDeleteAction extends BaseAction
{
    public function __invoke(EntityService $entityService, Participant $data)
    {
        $entityService->markAsDeleted($data, Participant::class);
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}