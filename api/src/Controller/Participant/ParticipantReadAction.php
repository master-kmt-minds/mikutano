<?php

namespace App\Controller\Participant;

use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\Participant;

class ParticipantReadAction extends BaseAction
{
    public function __invoke(Participant $data)
    {
        return $data;
    }

    public function getSecurityZone(): string
    {
        return Constants::SECZONE_BO_ZONE;
    }
}