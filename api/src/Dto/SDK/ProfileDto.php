<?php


namespace App\Dto\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\SDK\Profile\ProfileCreateAction;
use App\Controller\SDK\Profile\ProfileUpdateAction;

/**
 * @ApiResource(
 *     itemOperations={},
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/profiles",
 *              "controller"=ProfileCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "ProfileDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "profile" = {"type" = "string"},
 *                                  "nom" = {"type" = "string"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/profiles/{id}",
 *              "controller"=ProfileUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "ProfileDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "nom" = {"type" = "string"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          }
 *     }
 * )
 */
final class ProfileDto
{
    public $profile;

    public $nom;
}