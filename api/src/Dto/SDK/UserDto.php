<?php


namespace App\Dto\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Constants;
use App\Controller\SDK\User\UserCreateAction;
use App\Controller\SDK\User\UserUpdateAction;
use App\Controller\SDK\User\UserChangePasswordAction;
use App\Controller\SDK\User\UserUnsubscribeAction;
use App\Controller\SDK\User\UserConfirmAction;
use App\Controller\SDK\User\UserRecoverPasswordAction;
use App\Controller\SDK\User\UserResetPasswordAction;

/**
 * @ApiResource(
 *     itemOperations={},
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/users",
 *              "controller"=UserCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "userDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "nom" = {"type" = "string", "required"="true"},
 *                                  "prenom" = {"type" = "string", "required"="true"},
 *                                  "ville" = {"type" = "string", "required"="true"},
 *                                  "telephone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"},
 *                                  "profile" = {"type" = "string", "required"="true"},
 *                                  "active" = {"type" = "boolean", "required"="true"},
 *                                  "blocked" = {"type" = "boolean", "required"="true"},
 *                                  "password" = {"type" = "string", "required"="true"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/users/{id}",
 *              "controller"=UserUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "userDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "nom" = {"type" = "string", "required"="true"},
 *                                  "prenom" = {"type" = "string", "required"="true"},
 *                                  "ville" = {"type" = "string", "required"="true"},
 *                                  "telephone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"},
 *                                  "profile" = {"type" = "string", "required"="true"},
 *                                  "active" = {"type" = "boolean", "required"="true"},
 *                                  "blocked" = {"type" = "boolean"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "confirm"={
 *              "method"="PUT",
 *              "path"="/users/confirm/{token}",
 *              "controller"=UserConfirmAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "token",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "userDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "email" = {"type" = "string", "required"="true"},
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "change_password"={
 *              "method"="POST",
 *              "path"="/users/me/changepassword",
 *              "controller"=UserChangePasswordAction::class,
 *              "swagger_context"={
 *                  "parameters"={
 *                      {
 *                          "name" = "current_password",
 *                          "type" = "string",
 *                          "in" = "query",
 *                          "required" = "true",
 *                      },
 *                      {
 *                          "in" = "body",
 *                          "name" = "userDto",
 *                          "schema" = {
 *                              "type" = "object",
 *                              "properties" = {
 *                                  "password" = {"type"="string", "required"="true"},
 *                              }
 *                           },
 *                          "required" = "true",
 *                      }
 *                  }
 *              }
 *          },
 *          "user_unsubscribe"={
 *              "method"="POST",
 *              "path"="/users/me/unsubscribe",
 *              "controller"=UserUnsubscribeAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {}
 *              }
 *          },
 *          "recover"={
 *              "method"="PUT",
 *              "path"="/users/me/recover",
 *              "controller"=UserRecoverPasswordAction::class,
 *              "swagger_context"={
 *                  "parameters"={
 *                      {
 *                          "name" = "userDto",
 *                          "in" = "body",
 *                          "required" = "true",
 *                          "schema" = {
 *                              "properties" = {
 *                                  "email" = {"type" = "string", "required"="true"},
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "reset_password"={
 *              "method"="PUT",
 *              "path"="/users/me/resetpassword/{token}",
 *              "controller"=UserResetPasswordAction::class,
 *              "swagger_context"={
 *                  "parameters"={
 *                     {
 *                         "name" = "token",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                      {
 *                          "name" = "userDto",
 *                          "in" = "body",
 *                          "required" = "true",
 *                          "schema" = {
 *                              "properties" = {
 *                                  "email" = {"type" = "string", "required"="true"},
 *                                  "password" = {"type" = "string", "required"="true"},
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *     }
 * )
 */
final class UserDto
{
    /**
     * @var string
     */
    public $nom;

    /**
     * @var string
     */
    public $prenom;

    /**
     * @var string
     */
    public $ville;

    /**
     * @var string
     */
    public $telephone;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $profile = Constants::PROFILE_GUEST;

    public $active = false;

    public $blocked = false;

    /**
     * @var string
     */
    public $password;

    /**
     * UserDto constructor.
     */
    public function __construct()
    {
        $this->nom = '';
        $this->prenom = '';
        $this->ville = '';
        $this->telephone = '';
        $this->email = '';
        $this->password = '';
    }

}