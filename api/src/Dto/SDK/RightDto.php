<?php


namespace App\Dto\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\SDK\Right\RightCreateAction;
use App\Controller\SDK\Right\RightUpdateAction;

/**
 * @ApiResource(
 *     itemOperations={
 *     },
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/rights",
 *              "controller"=RightCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "UserDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "profile" = {"type" = "string", "required"="true"},
 *                                  "zone" = {"type" = "string", "required"="true"},
 *                                  "level" = {"type" = "integer", "required"="true"},
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/rights",
 *              "controller"=RightUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "UserDto",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "profile" = {"type" = "string", "required"="true"},
 *                                  "zone" = {"type" = "string", "required"="true"},
 *                                  "level" = {"type" = "integer", "required"="true"},
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *     }
 * )
 */
final class RightDto
{
    public $profile;

    public $zone;

    public $level;

}