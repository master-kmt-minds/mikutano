<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\SDK\User;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Participant\{ParticipantCreateAction, ParticipantDeleteAction, ParticipantListAction, ParticipantReadAction, ParticipantUpdateAction};
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"participant:read"}},
 *     attributes={"order"={"name": "asc"}},
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "path"="/participants/{id}",
 *              "controller"=ParticipantReadAction::class,
 *          },
 *          "delete"={
 *              "method"="DELETE",
 *              "path"="/participants/{id}",
 *              "controller"=ParticipantDeleteAction::class,
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/participants/{id}",
 *              "controller"=ParticipantUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "participant",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "name" = {"type" = "string", "required"="true"},
 *                                  "phone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          }
 *      },
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/participants",
 *              "controller"=ParticipantCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "participant",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "name" = {"type" = "string", "required"="true"},
 *                                  "phone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "list"={
 *              "method"="GET",
 *              "path"="/participants",
 *              "controller"=ParticipantListAction::class
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ParticipantRepository")
 * @ORM\Table(name="participants")
 */
class Participant extends ApiBaseEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"participant:read", "event:read"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"participant:read", "event:read"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"participant:read", "event:read"})
     */
    protected $email;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SDK\User")
     * @ORM\JoinColumn(name="created_by", nullable=false)
     */
    protected $user;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
