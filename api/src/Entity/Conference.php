<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\SDK\User;
use App\Controller\Conference\{ConferenceCreateAction, ConferenceDeleteAction, ConferenceListAction, ConferenceParticipantListAction, ConferenceReadAction, ConferenceSpeakerListAction, ConferenceUpdateAction, ConferenceReportAction};
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"event:read"}},
 *     attributes={"order"={"createdAt": "desc"}},
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "path"="/conferences/{id}",
 *              "controller"=ConferenceReadAction::class,
 *          },
 *          "delete"={
 *              "method"="DELETE",
 *              "path"="/conferences/{id}",
 *              "controller"=ConferenceDeleteAction::class,
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/conferences/{id}",
 *              "controller"=ConferenceUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "conference",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "title" = {"type" = "string", "required"="true"},
 *                                  "address" = {"type" = "string", "required"="true"},
 *                                  "dateStart" = {"type" = "string", "required"="true", "example" = "2019-11-02"},
 *                                  "hourStart" = {"type" = "string", "required"="true", "example" = "08:30"},
 *                                  "dateEnd" = {"type" = "string", "required"="true", "example" = "2019-11-02"},
 *                                  "hourEnd" = {"type" = "string", "required"="true", "example" = "08:30"},
 *                                  "price" = {"type" = "decimal", "required"="true", "example" = 15.00},
 *                                  "participants" = {"type" = "array", "items" = {"type" = "string"}, "required"="false"},
 *                                  "speakers" = {"type" = "array", "items" = {"type" = "string"}, "required"="false"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "confReport"={
 *              "method"="GET",
 *              "path"="/conferences/export/{id}",
 *              "controller"=ConferenceReportAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     }
 *                 }
 *              }
 *          }
 *      },
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/conferences",
 *              "controller"=ConferenceCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "conference",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "title" = {"type" = "string", "required"="true"},
 *                                  "address" = {"type" = "string", "required"="true"},
 *                                  "dateStart" = {"type" = "string", "required"="true", "example" = "2019-11-02"},
 *                                  "hourStart" = {"type" = "string", "required"="true", "example" = "08:30"},
 *                                  "dateEnd" = {"type" = "string", "required"="true", "example" = "2019-11-02"},
 *                                  "hourEnd" = {"type" = "string", "required"="true", "example" = "08:30"},
 *                                  "price" = {"type" = "decimal", "required"="true", "example" = 15.00},
 *                                  "participants" = {"type" = "array", "items" = {"type" = "string"}, "required"="true"},
 *                                  "speakers" = {"type" = "array", "items" = {"type" = "string"}, "required"="false"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "list"={
 *              "method"="GET",
 *              "path"="/conferences",
 *              "controller"=ConferenceListAction::class
 *          }
 *     },
 *     subresourceOperations={
 *          "listSpeakers"={
 *              "method"="GET",
 *              "path"="/conferences/{id}/speakers",
 *              "controller"=ConferenceSpeakerListAction::class,
 *          },
 *          "listParticipants"={
 *              "method"="GET",
 *              "path"="/conferences/{id}/participants",
 *              "controller"=ConferenceParticipantListAction::class,
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ConferenceRepository")
 * @ORM\Table(name="conferences")
 */
class Conference extends ApiBaseEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"event:read"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"event:read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $date_start;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $hour_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $date_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $hour_end;

    /**
     * @ORM\Column(type="decimal", options={"default"=0.0, "scale"=2})
     *
     * @Groups({"event:read"})
     */
    private $price;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"event:read"})
     */
    private $note;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SDK\User")
     * @ORM\JoinColumn(name="created_by")
     *
     * @Groups({"event:read"})
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Participant", cascade={"persist"})
     * @ApiSubresource()
     *
     * @Groups({"event:read"})
     */
    private $participants;

    /**
     * @ORM\ManyToMany(targetEntity="Speaker", cascade={"persist"})
     * @ApiSubresource()
     *
     * @Groups({"event:read"})
     */
    private $speakers;

    public function __construct()
    {
        parent::__construct();
        $this->participants = new ArrayCollection();
        $this->speakers = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     *
     * @Groups({"event:read"})
     */
    public function getDateStart(): ?string
    {
        return $this->date_start;
    }

    public function setDateStart(string $date_start): self
    {
        $this->date_start = $date_start;

        return $this;
    }

    /**
     * @return string|null
     *
     * @Groups({"event:read"})
     */
    public function getHourStart(): ?string
    {
        return $this->hour_start;
    }

    public function setHourStart(string $hour_start): self
    {
        $this->hour_start = $hour_start;

        return $this;
    }

    /**
     * @return string|null
     *
     * @Groups({"event:read"})
     */
    public function getDateEnd(): ?string
    {
        return $this->date_end;
    }

    public function setDateEnd(?string $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    /**
     * @return string|null
     *
     * @Groups({"event:read"})
     */
    public function getHourEnd(): ?string
    {
        return $this->hour_end;
    }

    public function setHourEnd(?string $hour_end): self
    {
        $this->hour_end = $hour_end;

        return $this;
    }
    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Participant[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): self
    {
        if ($this->participants->contains($participant)) {
            $this->participants->removeElement($participant);
        }

        return $this;
    }

    /**
     * @return Collection|Speaker[]
     */
    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function addSpeaker(Speaker $speaker): self
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers[] = $speaker;
        }

        return $this;
    }

    public function removeSpeaker(Speaker $speaker): self
    {
        if ($this->speakers->contains($speaker)) {
            $this->speakers->removeElement($speaker);
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
