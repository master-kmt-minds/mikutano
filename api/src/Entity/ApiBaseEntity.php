<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class ApiBaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"event:read", "participant:read", "speaker:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")

     * @Groups({"event:read", "participant:read", "speaker:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)

     * @Groups({"event:read", "participant:read", "speaker:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")

     * @Groups({""})
     */
    private $deleted;

    /**
     * ApiBaseEntity constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setDeleted(false);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtBfSaving() {
        $this->setCreatedAt(new \DateTime());
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtBfSaving() {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
}
