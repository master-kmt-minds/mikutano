<?php

namespace App\Entity\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\SDK\Setting\SettingsListAction;

/**
 * @ApiResource(
 *      itemOperations={},
 *      collectionOperations={
 *          "getbytypename"={
 *              "method"="GET",
 *              "path"="/settings/{typename}",
 *              "controller"=SettingsListAction::class,
 *              "defaults"={"_api_receive"=false},
 *              "swagger_context"={
 *                  "parameters"={
 *                     {
 *                         "name" = "typename",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     }
 *                  }
 *              },
 *          },
 *      }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\SDK\SettingsRepository")
 */
class Settings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $keyname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypename(): ?string
    {
        return $this->typename;
    }

    public function setTypename(string $typename): self
    {
        $this->typename = $typename;

        return $this;
    }

    public function getKeyname(): ?string
    {
        return $this->keyname;
    }

    public function setKeyname(string $keyname): self
    {
        $this->keyname = $keyname;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
