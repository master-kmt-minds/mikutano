<?php

namespace App\Entity\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Constants;
use App\Controller\SDK\User\UserListAction;
use App\Controller\SDK\User\UserMeAction;
use App\Controller\SDK\User\UserReadAction;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"read"}},
 *      denormalizationContext={"groups"={"write"}},
 *      itemOperations={
 *          "getone"={
 *              "method"="GET",
 *              "path"="/users/{id}",
 *              "controller"=UserReadAction::class,
 *              "swagger_context"={
 *                  "parameters"={
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     }
 *                  }
 *              }
 *          },
 *          "get_active_user"={
 *              "method"="GET",
 *              "path"="/users/active/me",
 *              "controller"=UserMeAction::class,
 *              "defaults"={"_api_receive"=false},
 *              "swagger_context"={
 *                  "parameters"={
 *
 *                  }
 *              }
 *          },
 *      },
 *      collectionOperations={
 *          "getall"={
 *              "method"="GET",
 *              "path"="/users",
 *              "controller"=UserListAction::class,
 *              "defaults"={"_api_receive"=false},
 *              "swagger_context"={
 *                  "parameters"={
 *
 *                  }
 *              },
 *          }
 *      }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SDK\UserRepository")
 * @ORM\Table(name="users", uniqueConstraints={@UniqueConstraint(name="users_uniq", columns={"email", "password"})})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=180, unique=false)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=180, unique=false)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=18, unique=false)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=80, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $profile = Constants::PROFILE_GUEST;

    /**
     * @var boolean User closed their account or not
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @var boolean User is blocked or not by admin
     * @ORM\Column(type="boolean")
     */
    private $blocked = false;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var string A tempory token
     * @ORM\Column(type="string", nullable=true)
     */
    private $temptoken;

    /**
     * @var \DateTime Validity date of the tempory token
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tokenvaliditydate;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille(string $ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        return array($this->profile);
    }

    /**
     * @return string
     */
    public function getProfile(): string
    {
        return $this->profile;
    }

    /**
     * @param string $profile
     */
    public function setProfile(string $profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param bool $blocked
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTemptoken(): ?string
    {
        return $this->temptoken;
    }

    public function setTemptoken(?string $temptoken): self
    {
        $this->temptoken = $temptoken;

        return $this;
    }

    public function getTokenvaliditydate(): ?\DateTime
    {
        return $this->tokenvaliditydate;
    }

    public function setTokenvaliditydate(?\DateTime $tokenvaliditydate): self
    {
        $this->tokenvaliditydate = $tokenvaliditydate;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getBlocked(): ?bool
    {
        return $this->blocked;
    }
}
