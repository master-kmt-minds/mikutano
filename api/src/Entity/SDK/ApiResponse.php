<?php


namespace App\Entity\SDK;


class ApiResponse
{
    /*******************************************************************
     * Constructor
     *******************************************************************/
    public function __construct()
    {
        $this->code = 1;
        $this->message = '';
        $this->context = array ();
        $this->data = array ();
    }

    /*******************************************************************
     * Entity attributes
     *******************************************************************/
    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $context;

    /**
     * @var array
     */
    private $data;

    /*******************************************************************
     * Getters and Setters
     *******************************************************************/
    public function getCode(): int
    {
        return $this->code;
    }
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /********************/
    public function getMessage(): string
    {
        return $this->message;
    }
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /********************/
    public function getContext()
    {
        return $this->context;
    }
    public function setContext($context)
    {
        $this->context = $context;
    }

    /********************/
    public function getData()
    {
        return $this->data;
    }
    public function setData($data)
    {
        $this->data = $data;
    }

}