<?php

namespace App\Entity\SDK;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Controller\SDK\Right\RightsProfileAction;
use App\Controller\SDK\Right\RightsListAction;

/**
 * @ApiResource(
 *      itemOperations={},
 *      collectionOperations={
 *          "getallbyprofile"={
 *              "method"="GET",
 *              "path"="/rights/{profile}",
 *              "controller"=RightsProfileAction::class,
 *              "defaults"={"_api_receive"=false},
 *              "swagger_context"={
 *                  "parameters"={
 *                     {
 *                         "name" = "profile",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     }
 *                  }
 *              },
 *          },
 *          "getall"={
 *              "method"="GET",
 *              "path"="/rights",
 *              "controller"=RightsListAction::class,
 *              "defaults"={"_api_receive"=false},
 *              "swagger_context"={
 *                  "parameters"={}
 *              },
 *          }
 *      }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SDK\RightsRepository")
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="rgt_un", columns={"profile", "zone"})})
 */
class Rights
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $profile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zone;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfile(): ?string
    {
        return $this->profile;
    }

    public function setProfile(string $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getZone(): ?string
    {
        return $this->zone;
    }

    public function setZone(string $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
