<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\SDK\User;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Speaker\{SpeakerCreateAction, SpeakerDeleteAction, SpeakerListAction, SpeakerReadAction, SpeakerUpdateAction};
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"speaker:read"}},
 *     attributes={"order"={"name": "asc"}},
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "path"="/speakers/{id}",
 *              "controller"=SpeakerReadAction::class,
 *          },
 *          "delete"={
 *              "method"="DELETE",
 *              "path"="/speakers/{id}",
 *              "controller"=SpeakerDeleteAction::class,
 *          },
 *          "update"={
 *              "method"="PUT",
 *              "path"="/speakers/{id}",
 *              "controller"=SpeakerUpdateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "id",
 *                         "in" = "path",
 *                         "required" = "true",
 *                         "type" = "string"
 *                     },
 *                     {
 *                         "name" = "speaker",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "name" = {"type" = "string", "required"="true"},
 *                                  "phone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"},
 *                                  "photo" = {"type" = "string", "required"="false"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          }
 *      },
 *     collectionOperations={
 *          "create"={
 *              "method"="POST",
 *              "path"="/speakers",
 *              "controller"=SpeakerCreateAction::class,
 *              "swagger_context" = {
 *                 "parameters" = {
 *                     {
 *                         "name" = "speaker",
 *                         "in" = "body",
 *                         "required" = "true",
 *                         "schema" = {
 *                              "properties" = {
 *                                  "name" = {"type" = "string", "required"="true"},
 *                                  "phone" = {"type" = "string", "required"="true"},
 *                                  "email" = {"type" = "string", "required"="true"},
 *                                  "photo" = {"type" = "string", "required"="false"}
 *                              }
 *                         }
 *                     }
 *                 }
 *              }
 *          },
 *          "list"={
 *              "method"="GET",
 *              "path"="/speakers",
 *              "controller"=SpeakerListAction::class
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SpeakerRepository")
 * @ORM\Table(name="speakers")
 */
class Speaker extends ApiBaseEntity
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"event:read"})
     */
    protected $photo;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"speaker:read", "event:read"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"speaker:read", "event:read"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"speaker:read", "event:read"})
     */
    protected $email;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SDK\User")
     * @ORM\JoinColumn(name="created_by", nullable=false)
     */
    protected $user;

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
