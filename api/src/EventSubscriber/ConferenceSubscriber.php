<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Constants;
use App\Entity\Conference;
use App\Entity\SDK\User;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ConferenceSubscriber implements EventSubscriberInterface
{
    private $user;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        if ($tokenStorage->getToken() === null || !($tokenStorage->getToken()->getUser() instanceof User)) {
            $this->user = null;
        } else {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['setUserId', EventPriorities::PRE_VALIDATE],
                ['checkEventDates', EventPriorities::PRE_VALIDATE],
                ['checkEventValidity', EventPriorities::PRE_VALIDATE],
            ],
        ];
    }

    public function setUserId(ViewEvent $viewEvent)
    {
        $conference = $viewEvent->getControllerResult();
        $method = $viewEvent->getRequest()->getMethod();

        if ($conference instanceof Conference && (Request::METHOD_POST == $method || Request::METHOD_PUT == $method)) {
            //here we supposed security listener has already checked user identity; so need to check here
            $conference->setUser($this->user);
        }
    }

    public function checkEventDates(ViewEvent $viewEvent)
    {
        $conference = $viewEvent->getControllerResult();
        $method = $viewEvent->getRequest()->getMethod();
        $now = new \DateTime();

        if ($conference instanceof Conference && (Request::METHOD_POST == $method || Request::METHOD_PUT == $method)) {
            if (!preg_match(Constants::REGEX_DATE, $conference->getDateStart())) {
                throw new BadRequestHttpException("dateStart not valid");
            }
            if (!preg_match(Constants::REGEX_TIME, $conference->getHourStart())) {
                throw new BadRequestHttpException("hourStart not valid");
            }
            if (!preg_match(Constants::REGEX_DATE, $conference->getDateEnd())) {
                throw new BadRequestHttpException("dateEnd not valid");
            }
            if (!preg_match(Constants::REGEX_TIME, $conference->getHourEnd())) {
                throw new BadRequestHttpException("hourEnd not valid");
            }

            try {
                $debut = new \DateTime($conference->getDateStart() . ' ' . $conference->getHourStart());
            } catch (Exception $exception) {
                throw new BadRequestHttpException("Unable to create Start date. Ensure you gave good format.");
            }

            try {
                $fin = new \DateTime($conference->getDateEnd() . ' ' . $conference->getHourEnd());
            } catch (Exception $exception) {
                throw new BadRequestHttpException("Unable to create End date. Ensure you gave good format.");
            }
            if ($debut > $fin) {
                throw new BadRequestHttpException("Start date is higher than end date.");
            }
        }

        if ($conference instanceof Conference && Request::METHOD_POST == $method) {
            if ($debut < $now) {
                throw new BadRequestHttpException("You can not create a past event. Check dates");
            }
        }

        if ($conference instanceof Conference && Request::METHOD_PUT == $method) {
            if ($fin < $now) {
                throw new BadRequestHttpException("You can not modify a past event.");
            }
        }
    }
    public function checkEventValidity(ViewEvent $viewEvent)
    {
        $conference = $viewEvent->getControllerResult();
        $method = $viewEvent->getRequest()->getMethod();

        if ($conference instanceof Conference && Request::METHOD_PUT == $method) {
            if ($conference->getDeleted() === true) {
                throw new NotFoundHttpException("An error occurs. No resource found.");
            }
        }
    }
}