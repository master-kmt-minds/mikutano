<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Constants;
use App\Entity\Conference;
use App\Entity\Participant;
use App\Entity\SDK\User;
use App\Entity\Speaker;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ParticipantSubscriber implements EventSubscriberInterface
{
    private $user;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        if ($tokenStorage->getToken() === null || ! ($tokenStorage->getToken()->getUser() instanceof User)) {
            $this->user = null;
        }
        else {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['setUserId', EventPriorities::PRE_VALIDATE],
            ],
        ];
    }

    public function setUserId(ViewEvent $viewEvent) {
        $participant = $viewEvent->getControllerResult();
        $method = $viewEvent->getRequest()->getMethod();

        if ($participant instanceof Participant && (Request::METHOD_POST == $method || Request::METHOD_PUT == $method)) {
            //here we supposed security listener has already checked user identity; so need to check here
            $participant->setUser($this->user);
        }
    }
}