<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 20/03/19
 * Time: 12:51
 */

namespace App\EventSubscriber;


use App\Constants;
use App\Controller\SDK\BaseAction;
use App\Entity\SDK\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SecurityListener implements EventSubscriberInterface
{
    private $entityManager;
    private $user;

    public function __construct(EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $em;

        if ($tokenStorage->getToken() === null || ! ($tokenStorage->getToken()->getUser() instanceof User)) {
            $this->user = null;
        }
        else {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if ($controller instanceof BaseAction)
        {
            if($this->user === null)
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);

            $right = $this->getSecurityAccess($this->user->getProfile(), $controller->getSecurityZone());

            if ($right == Constants::SECURITY_NOACCESS)
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);

            if (($event->getRequest()->getMethod() != 'GET') && ($right == Constants::SECURITY_READONLY))
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);
        }
        else if (array_key_exists(0, $controller) && isset($controller[0]) && $controller[0] instanceof BaseAction)
        {
            $controller = $controller[0];

            if($this->user === null)
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);

            $right = $this->getSecurityAccess($this->user->getProfile(), $controller->getSecurityZone());

            if ($right == Constants::SECURITY_NOACCESS)
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);

            if (($event->getRequest()->getMethod() != 'GET') && ($right == Constants::SECURITY_READONLY))
                throw new AccessDeniedHttpException(Constants::MSG_NOAUTHORISED);
        }

    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    private function getSecurityAccess($profile, $securityZone): int
    {
        $entity = $this->entityManager
            ->getRepository('App\Entity\SDK\Rights')
            ->findOneBy( array('profile' => $profile, 'zone' => $securityZone));

        if ($entity === null) {
            return 0;
        }

        return $entity->getLevel();
    }
}