<?php


namespace App\EventSubscriber;

use App\Constants;
use App\Exception\AccountClosedException;
use App\Exception\AccountDeletedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class LoginListener implements UserCheckerInterface
{

    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof UserInterface) {
            return;
        }

        if ($user->isActive() === false) {
            throw new AccountClosedException(Constants::MSG_USER_NOT_ACTIVED);

        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @throws AccountStatusException
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof UserInterface) {
            return;
        }

        if ($user->isBlocked() === true) {
            throw new AccountDeletedException(Constants::MSG_USER_BEEN_BLOCKED);
        }
    }
}