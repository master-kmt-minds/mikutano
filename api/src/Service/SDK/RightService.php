<?php


namespace App\Service\SDK;


use App\Constants;
use App\Dto\SDK\RightDto;
use App\Entity\SDK\ApiResponse;
use App\Entity\SDK\Rights;
use App\Entity\SDK\Settings;

class RightService extends BaseService
{
    public function listRights(ApiResponse $apiResponse, ProfileService $profileService)
    {
        $check = true;

        if ($check) {
            $profileResult = $profileService->list($apiResponse);
            if (empty($profileResult->getData())) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("No profile found");
                $check = false;
            }
        }

        if ($check) {
            $listProfiles = $profileResult->getData();
            $rightsByProfile = [];
            foreach ($listProfiles as $profile) {
                $rightsRes = $this->listRightsByProfile($apiResponse, $profile['profile']);
                if ($rightsRes->getCode() === 0) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("Rights not found for the profile '".$profile['nom']."'(".$profile['profile'].")");
                    $check = false;
                    break 1;
                }
                $rightsByProfile[$profile['profile']] = $rightsRes->getData();
            }
        }

        if ($check) {
            $apiResponse->setData($rightsByProfile);
        }

        return $apiResponse;
    }

    public function listRightsByProfile(ApiResponse $apiResponse, string $profile)
    {
        $check = true;

        if ($check) {
            if ($profile == "" || strlen(trim($profile)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            $profileE = $this->em->getRepository(Settings::class)
                ->findOneBy(["typename" =>Constants::SETTINGS_PROFILE, "keyname" => strip_tags($profile)]);

            if ($profileE === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile does not exist");
                $check = false;
            }
        }

        if ($check) {
            $rights = $this->em->getRepository(Rights::class)
                ->listRightsByProfile(strip_tags($profile));

            if (empty($rights)) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile has no set rights");
                $check = false;
            }
        }

        if ($check) {
            $apiResponse->setContext(['profile' => $profile]);
            $apiResponse->setData($rights);
        }

        return $apiResponse;
    }

    public function create(ApiResponse $apiResponse, RightDto $data)
    {
        $check = true;

        if ($check) {
            if ($data->profile == "" || strlen(trim($data->profile)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            $pf = preg_replace('/\s/', '', trim(strip_tags($data->profile)));
            $profile = $this->em->getRepository(Settings::class)
                ->findOneBy(["typename" =>Constants::SETTINGS_PROFILE, "keyname" => $pf]);

            if ($profile === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile does not exist");
                $check = false;
            }
        }

        if ($check) {
            if ($data->zone == "" || strlen(trim($data->zone)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The zone should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            $nZone = preg_replace('/\s/', '', trim(strip_tags($data->zone)));
            $secZone = $this->em->getRepository(Settings::class)
                ->findOneBy(["typename" =>Constants::SETTINGS_SECZONE, "keyname" => $nZone]);

            if ($secZone === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The security zone does not exist");
                $check = false;
            }
        }

        if ($check) {
            if (!is_int($data->level) || $data->level < 0 || $data->level > 2) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The level should be a valid int (0, 1 or 2)");
                $check = false;
            }
        }

        if ($check) {
            $right = $this->em->getRepository(Rights::class)
                ->findOneBy(["profile" => $pf, "zone" => $nZone]);
            if ($right !== null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The couple (".$data->profile.", ".$data->zone.") already exist");
                $check = false;
            }
        }

        if ($check) {
            $right = new Rights();
            $right->setProfile(strip_tags($pf))
                ->setZone(strip_tags($nZone))
                ->setLevel(strip_tags($data->level));

            if ($this->dataPersister->supports($right)) {
                $this->dataPersister->persist($right);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "nom" => $right->getProfile(),
                    "zone" => $right->getZone(),
                    "level" => $right->getLevel()
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function update(ApiResponse $apiResponse, RightDto $data)
    {
        $check = true;

        if ($check) {
            if ($data->profile == "" || strlen(trim($data->profile)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            if ($data->zone == "" || strlen(trim($data->zone)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The zone should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            if (!is_int($data->level) || $data->level < 0 || $data->level > 2) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The level should be a valid int (0, 1 or 2)");
                $check = false;
            }
        }

        if ($check) {
            $pf = preg_replace('/\s/', '', trim(strip_tags($data->profile)));
            $nZone = preg_replace('/\s/', '', trim(strip_tags($data->zone)));
        }

        if ($check) {
            $right = $this->em->getRepository(Rights::class)
                ->findOneBy(["profile" => $pf, "zone" => $nZone]);
            if ($right === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The couple (".$data->profile.", ".$data->zone.") does not exist");
                $check = false;
            }
        }

        if ($check) {
            $right->setProfile(strip_tags($pf))
                ->setZone(strip_tags($nZone))
                ->setLevel(strip_tags($data->level));

            if ($this->dataPersister->supports($right)) {
                $this->dataPersister->persist($right);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "nom" => $right->getProfile(),
                    "zone" => $right->getZone(),
                    "level" => $right->getLevel()
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }
}