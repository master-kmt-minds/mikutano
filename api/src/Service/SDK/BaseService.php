<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 20/03/19
 * Time: 12:58
 */

namespace App\Service\SDK;


use ApiPlatform\Core\DataPersister\ChainDataPersister;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use App\Constants;
use App\Entity\SDK\ApiResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class BaseService
{
    /**
     * @var CollectionDataProviderInterface
     */
    protected $collectionDataProvider;

    /**
     * @var ItemDataProviderInterface
     */
    protected $itemDataProviderInterface;

    /**
     * @var DataPersisterInterface
     */
    protected $dataPersister;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var HelperService
     */
    protected $helperService;

    /**
     * UserService constructor.
     * @param CollectionDataProviderInterface $collectionDataProvider
     */
    public function __construct(CollectionDataProviderInterface $collectionDataProvider,
                                ItemDataProviderInterface $itemDataProviderInterface,
                                DataPersisterInterface $dataPersister,
                                EntityManagerInterface $entityManager,
                                UserPasswordEncoderInterface $encoder,
                                Security $security,
                                HelperService $helper)
    {
        $this->collectionDataProvider = $collectionDataProvider;
        $this->itemDataProviderInterface = $itemDataProviderInterface;
        $this->dataPersister = $dataPersister;
        $this->encoder = $encoder;
        $this->em = $entityManager;
        $this->security = $security;
        $this->helperService = $helper;
    }

}