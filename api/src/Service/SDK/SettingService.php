<?php


namespace App\Service\SDK;


use App\Constants;
use App\Entity\SDK\ApiResponse;
use App\Entity\SDK\Rights;
use App\Entity\SDK\Settings;

class SettingService extends BaseService
{

    public function listSettingsByType(ApiResponse $apiResponse, string $type)
    {
        $check = true;

        if ($check) {
            if ($type == "" || strlen(trim($type)) == 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The type should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            $settings = $this->em->getRepository(Settings::class)
                ->listByType($type, ["keyname"=>"keyname", "value"=>"value"]);

            if (empty($settings)) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The setting (".$type.") does not exist");
                $check = false;
            }
        }

        if ($check) {
            $apiResponse->setContext(['type' => $type]);
            $apiResponse->setData($settings);
        }

        return $apiResponse;
    }

}