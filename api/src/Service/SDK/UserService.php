<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 09/02/19
 * Time: 18:00
 */

namespace App\Service\SDK;


use App\Constants;
use App\DTO\SDK\UserDto;
use App\Entity\SDK\ApiResponse;
use App\Entity\SDK\Profiles;
use App\Entity\SDK\Rights;
use App\Entity\SDK\Settings;
use App\Entity\SDK\User;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserService extends BaseService
{
    private function verifyData(UserDto $data)
    {
        $isOk = true;
        $result = [];
        $result['code'] = 1;
        $result['message'] = "";

        if ($isOk) {
            if (strlen(filter_var(trim($data->nom), FILTER_SANITIZE_STRING)) === 0) {
                $result['code'] = 0;
                $result['message'] = "The nom should be a valid string";
                $isOk = false;
            }
        }
        if ($isOk) {
            if (strlen(filter_var(trim($data->prenom), FILTER_SANITIZE_STRING)) === 0) {
                $result['code'] = 0;
                $result['message'] = "The prenom should be a valid string";
                $isOk = false;
            }
        }
        if ($isOk) {
            if (strlen(filter_var(trim($data->ville), FILTER_SANITIZE_STRING)) === 0) {
                $result['code'] = 0;
                $result['message'] = "The ville should be a valid string";
                $isOk = false;
            }
        }
        if ($isOk) {
            if (!filter_var(trim($data->telephone), FILTER_VALIDATE_REGEXP, ["options" => ["regexp" => Constants::CM_PHONE_NUMBER_REGEX]])) {
                $result['code'] = 0;
                $result['message'] = "The telephone should be a valid Cameroon phone number";
                $isOk = false;
            }
        }
        if ($isOk) {
            if (!filter_var(trim($data->email), FILTER_VALIDATE_EMAIL)) {
                $result['code'] = 0;
                $result['message'] = "The email should be a valid e-mail address";
                $isOk = false;
            }
        }
        if ($isOk) {
            if (strlen(filter_var(trim($data->profile), FILTER_SANITIZE_STRING)) === 0) {
                $result['code'] = 0;
                $result['message'] = "The profile should be a valid string";
                $isOk = false;
            }
        }
        return $result;
    }

    public function create(ApiResponse $apiResponse, UserDto $data)
    {
        $result = $this->verifyData($data);
        if ($result['code'] == 0) {
            $apiResponse->setCode($result['code']);
            $apiResponse->setMessage($result['message']);
        } else {
            $check = true;

            if ($check) {
                if ($data->password !== "" && preg_match(Constants::PASSWORD_REGEX, $data->password) !== 1) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("The password does not meet requirement");
                    $check = false;
                }
            }

            if ($check) {
                $pf = preg_replace('/\s/', '', trim(strip_tags($data->profile)));
                $profile = $this->em->getRepository(Settings::class)
                    ->findOneBy(["typename" => Constants::SETTINGS_PROFILE, "keyname" => $pf]);

                if ($profile === null) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("The profile does not exist");
                    $check = false;
                }
            }

            if ($check) {
                $uTemp = $this->em->getRepository(User::class)
                    ->findOneBy(["email" => $data->email]);

                if ($uTemp != null) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("A user with the same email already exists");
                    $check = false;
                }
            }

            if ($check) {
                $user = new User();
                $user->setNom(trim(filter_var($data->nom), FILTER_SANITIZE_STRING));
                $user->setPrenom(trim(filter_var($data->prenom), FILTER_SANITIZE_STRING));
                $user->setVille(trim(filter_var($data->ville), FILTER_SANITIZE_STRING));
                $user->setTelephone($data->telephone);
                $user->setEmail(trim(filter_var($data->email), FILTER_VALIDATE_EMAIL));
                $user->setProfile($pf);
                $user->setActive($data->active);
                $user->setBlocked($data->blocked);
                $user->setPassword($this->encoder->encodePassword($user, $data->password));
                $user->setTemptoken($this->helperService->generateToken());
                $user->setTokenvaliditydate($this->helperService->genTokenValidity(Constants::TOKEN_VALIDITY_REGISTRATION));

                if ($this->dataPersister->supports($user)) {
                    $this->dataPersister->persist($user);
                    $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);

                    $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                        Constants::MAIL_WELCOME_OBJECT, 'user/welcome',
                        [
                            'params' => array(
                                'name'=>$user->getNom(),
                                'protocol'=>getenv('APP_PROTOCOL'),
                                'link' => getenv('APP_BO_URL').'confirm?email='.$user->getEmail().'&token='.$user->getTemptoken()
                            )
                        ]
                    );

                    if (!is_numeric($mail)) {
                        $apiResponse->setCode(0);
                        $apiResponse->setMessage($mail);
                    }

                    $apiResponse->setData([
                        "nom" => $user->getNom(),
                        "prenom" => $user->getPrenom(),
                        "ville" => $user->getVille(),
                        "telephone" => $user->getTelephone(),
                        "email" => $user->getEmail(),
                        "profile" => $user->getProfile(),
                        "active" => $user->isActive(),
                        "blocked" => $user->isBlocked(),
                    ]);
                } else {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
                }
            }
        }

        return $apiResponse;
    }

    public function update(ApiResponse $apiResponse, UserDto $data, int $id)
    {
        $result = $this->verifyData($data);
        if ($result['code'] == 0) {
            $apiResponse->setCode($result['code']);
            $apiResponse->setMessage($result['message']);
        } else {
            $check = true;

            if ($check) {
                /**
                 * @var User $user
                 */
                $user = $this->em->getRepository(User::class)
                    ->findOneBy(["id" => $id]);
                if ($user === null) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("This user does not exist");
                    $check = false;
                }
            }

            if ($check) {
                $pf = preg_replace('/\s/', '', trim(strip_tags($data->profile)));
                $profile = $this->em->getRepository(Settings::class)
                    ->findOneBy(["typename" => Constants::SETTINGS_PROFILE, "keyname" => $pf]);

                if ($profile === null) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage("The profile does not exist");
                    $check = false;
                }
            }

            if ($check) {
                if ($data->email != $user->getEmail()) {
                    $uTemp = $this->em->getRepository(User::class)
                        ->findBy(["email" => $data->email]);

                    if ($uTemp !== null) {
                        $apiResponse->setCode(0);
                        $apiResponse->setMessage("A user with this email already exist");
                        $check = false;
                    }
                }
            }

            //TODO when frontend user need to change they data (check if current user = user to be updated)

            if ($check) {
                $isBlocked = false;
                $isUnblocked = false;
                if ($data->blocked !== $user->isBlocked()) {
                    if ($data->blocked) {
                        $isBlocked = true;
                    }
                    if (!$data->blocked) {
                        $isUnblocked = true;
                    }
                }
            }

            if ($check) {
                $user->setNom(trim(filter_var($data->nom), FILTER_SANITIZE_STRING));
                $user->setPrenom(trim(filter_var($data->prenom), FILTER_SANITIZE_STRING));
                $user->setVille(trim(filter_var($data->ville), FILTER_SANITIZE_STRING));
                $user->setTelephone($data->telephone);
                $user->setEmail(trim(filter_var($data->email), FILTER_VALIDATE_EMAIL));
                $user->setProfile($pf);
                $user->setActive($data->active);
                $user->setBlocked($data->blocked);

                if ($this->dataPersister->supports($user)) {
                    $this->dataPersister->persist($user);
                    $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);

                    if ($isBlocked) {
                        $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                            Constants::MAIL_LOCK_USER_OBJECT, 'user/lockuser'
                        );

                        if (!is_numeric($mail)) {
                            $apiResponse->setCode(0);
                            $apiResponse->setMessage($mail);
                        }
                    }

                    if ($isUnblocked) {
                        $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                            Constants::MAIL_UNLOCK_USER_OBJECT, 'user/unlockuser'
                        );

                        if (!is_numeric($mail)) {
                            $apiResponse->setCode(0);
                            $apiResponse->setMessage($mail);
                        }
                    }

                    $apiResponse->setContext([
                        "id" => $id
                    ]);
                    $apiResponse->setData([
                        "id" => $id,
                        "nom" => $user->getNom(),
                        "prenom" => $user->getPrenom(),
                        "ville" => $user->getVille(),
                        "telephone" => $user->getTelephone(),
                        "email" => $user->getEmail(),
                        "profile" => $user->getProfile(),
                        "active" => $user->isActive(),
                        "blocked" => $user->isBlocked(),
                    ]);
                } else {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
                }
            }
        }

        return $apiResponse;
    }

    public function unsubscribe(ApiResponse $apiResponse, int $id)
    {
        $check = true;

        if ($check) {
            /**
             * @var User $user
             */
            $user = $this->em->getRepository(User::class)
                ->findOneBy(["id" => $id]);
            if ($user === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This user does not exist");
                $check = false;
            }
        }

        if ($check) {
            $user->setActive(false);

            if ($this->dataPersister->supports($user)) {
                $this->dataPersister->persist($user);
                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);

                $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                    Constants::MAIL_UNSUBSCRIBE_OBJECT, 'user/unsubscribe',
                    [
                        'params' => array(
                            'name'=>$user->getNom(),
                            'token'=>$user->getTemptoken(),
                            'protocol'=>getenv('APP_PROTOCOL'),
                            'link' => getenv('APP_BO_URL')
                        )
                    ]
                );

                if (!is_numeric($mail)) {
                    $apiResponse->setCode(0);
                    $apiResponse->setMessage($mail);
                }

                $apiResponse->setContext([
                    "id" => $id
                ]);
                $apiResponse->setData([
                    "id" => $id,
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function get(ApiResponse $apiResponse, int $id)
    {
        $check = true;

        $user = $this->em->getRepository(User::class)
            ->findOneBy(array("id" => $id));

        if ($user === null) {
            $apiResponse->setCode(0);
            $apiResponse->setMessage("This user does not exist");
            $check = false;
        }

        if ($check) {
            $apiResponse->setContext([
                "id" => $id
            ]);

            if ($user !== null) {
                $apiResponse->setData([
                    "id" => $id,
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            }
        }

        return $apiResponse;
    }

    public function listAll(ApiResponse $apiResponse)
    {
        $users = $this->em->getRepository(User::class)
            ->listAll();

        if (!empty($users)) {
            $apiResponse->setData($users);
        }

        return $apiResponse;
    }

    public function changePassword(ApiResponse $apiResponse, UserDto $data, int $id, string $oldPassword)
    {
        $check = true;

        if ($check) {
            $user = $this->em->getRepository(User::class)
                ->findOneBy(["id" => $id]);
            if ($user === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This user does not exist");
                $check = false;
            }
        }

        if ($check) {
            if (!$this->encoder->isPasswordValid($user, $oldPassword)) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("Previous password does not match");
                $check = false;
            }
        }

        if ($check) {
            if ($data->password !== "" && preg_match(Constants::PASSWORD_REGEX, $data->password) !== 1) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The password does not meet requirement");
                $check = false;
            }
        }

        if ($check) {
            $user->setPassword($this->encoder->encodePassword($user, $data->password));

            if ($this->dataPersister->supports($user)) {
                $this->dataPersister->persist($user);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "id" => $id,
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function confirm(ApiResponse $apiResponse, UserDto $data, string $token)
    {
        $check = true;

        if ($check) {
            /**
             * @var User $user
             */
            $user = $this->em->getRepository(User::class)
                ->findOneBy(["email" => $data->email]);
            if ($user === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This user does not exist");
                $check = false;
            }
        }

        if ($check) {
            if ($user->getTemptoken() != $token) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The token does not exist");
                $check = false;
            }
        }

        if ($check) {
            if (!$this->helperService->checkTokenValidity($user->getTokenvaliditydate())) {
                $apiResponse->setMessage("The token is not valid anymore. A new link has been sent to you, please check your emails and follow instructions.");

                $user->setTemptoken($this->helperService->generateToken());
                $user->setTokenvaliditydate($this->helperService->genTokenValidity(Constants::TOKEN_VALIDITY_REGISTRATION));

                $this->dataPersister->persist($user);

                $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                    Constants::MAIL_WELCOME_OBJECT, 'user/welcome',
                    [
                        'params' => array(
                            'name'=>$user->getNom(),
                            'token'=>$user->getTemptoken(),
                            'protocol'=>getenv('APP_PROTOCOL'),
                            'link' => getenv('APP_BO_URL')
                        )
                    ]
                );

                if (!is_numeric($mail)) {
                    $apiResponse->setMessage($mail);
                }
                $apiResponse->setCode(0);
                $check = false;
            }
        }

        if ($check) {
            $user->setTemptoken(null);
            $user->setTokenvaliditydate(null);
            $user->setActive(true);

            if ($this->dataPersister->supports($user)) {
                $this->dataPersister->persist($user);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "id" => $user->getId(),
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function recover(ApiResponse $apiResponse, UserDto $data)
    {
        $check = true;

        if ($check) {
            /**
             * @var User $user
             */
            $user = $this->em->getRepository(User::class)
                ->findOneBy(["email" => $data->email]);
            if ($user === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This user does not exist");
                $check = false;
            }
        }

        if ($check) {
            $user->setActive(false);
            $user->setTemptoken($this->helperService->generateToken());
            $user->setTokenvaliditydate($this->helperService->genTokenValidity(Constants::TOKEN_VALIDITY_RESET_PASSWORD));
        }

        if ($check) {
            if ($this->dataPersister->supports($user)) {
                $this->dataPersister->persist($user);
                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);

                $mail = $this->helperService->sendMail(Constants::MAIL_DEFAULT_SENDER_MAIL, $user->getEmail(),
                    Constants::MAIL_RESET_PASSWORD_OBJECT, 'user/resetpassword',
                    [
                        'params' => array(
                            'name'=>$user->getNom(),
                            'protocol'=>getenv('APP_PROTOCOL'),
                            'link' => getenv('APP_BO_URL').'reset_password?email='.$user->getEmail().'&token='.$user->getTemptoken()
                        )
                    ]
                );

                if (!is_numeric($mail)) {
                    $apiResponse->setMessage($mail);
                }

                $apiResponse->setData([
                    "id" => $user->getId(),
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function resetPassword(ApiResponse $apiResponse, UserDto $data, string $token)
    {
        $check = true;

        if ($check) {
            /**
             * @var User $user
             */
            $user = $this->em->getRepository(User::class)
                ->findOneBy(["email" => $data->email]);
            if ($user === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This user does not exist");
                $check = false;
            }
        }

        if ($check) {
            if ($user->getTemptoken() != $token) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The token does not exist");
                $check = false;
            }
        }

        if ($check) {
            if (!$this->helperService->checkTokenValidity($user->getTokenvaliditydate())) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The token is not valid.");
                $check = false;
            }
        }

        if ($check) {
            if ($data->password !== "" && preg_match(Constants::PASSWORD_REGEX, $data->password) !== 1) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The password does not meet requirement");
                $check = false;
            }
        }

        if ($check) {
            $user->setTemptoken(null);
            $user->setTokenvaliditydate(null);
            $user->setActive(true);
            $user->setPassword($this->encoder->encodePassword($user, $data->password));

            if ($this->dataPersister->supports($user)) {
                $this->dataPersister->persist($user);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "id" => $user->getId(),
                    "nom" => $user->getNom(),
                    "prenom" => $user->getPrenom(),
                    "ville" => $user->getVille(),
                    "telephone" => $user->getTelephone(),
                    "email" => $user->getEmail(),
                    "profile" => $user->getProfile(),
                    "active" => $user->isActive(),
                    "blocked" => $user->isBlocked(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

}