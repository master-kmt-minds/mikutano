<?php


namespace App\Service\SDK;


use App\Constants;
use Twig\Environment;

class HelperService
{
    private $mailer;
    private $twig;

    /**
     * HelperService constructor.
     * @param \Swift_Mailer $mailer
     * @param Environment $twig
     */
    public function __construct(\Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendMail(string $from, string $to, string $object, string $template, array $params = [])
    {
        try {
            $message = (new \Swift_Message($object))
                ->setFrom($from)
                ->setTo($to)
                ->setBody(
                    $this->twig->render(
                    // templates/emails/registration.html.twig
                        'email/' . $template . '.html.twig',
                        $params
                    ),
                    'text/html'
                )/*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'emails/registration.txt.twig',
                        ['name' => $name]
                    ),
                    'text/plain'
                )
                */
            ;
            return $this->mailer->send($message);
        } catch (\Twig_Error_Loader $e) {
            return $e->getRawMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getRawMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getRawMessage();
        }
    }

    public function generateToken()
    {
        $token = openssl_random_pseudo_bytes(Constants::GENERIC_TOKEN_LENGTH);
        return ($token === false) ? $this->generateToken() : bin2hex($token);
    }

    public function genTokenValidity(int $limitValidity): \DateTime
    {
        return (new \DateTime())->add(new \DateInterval('PT'.$limitValidity.'M'));
    }

    public function checkTokenValidity(?\DateTime $tokenvalidity): bool
    {
        if (is_null($tokenvalidity))
            return false;

        $currentDate = new \DateTime();
        return ($tokenvalidity >= $currentDate);
    }
}