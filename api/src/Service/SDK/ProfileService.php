<?php


namespace App\Service\SDK;


use App\Constants;
use App\Dto\SDK\ProfileDto;
use App\Entity\SDK\ApiResponse;
use App\Entity\SDK\Rights;
use App\Entity\SDK\Settings;

class ProfileService extends BaseService
{
    public function create(ApiResponse $apiResponse, ProfileDto $data)
    {
        $check = true;

        if ($check) {
            if (strlen(filter_var(trim($data->nom), FILTER_SANITIZE_STRING)) === 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The nom should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            if (trim($data->profile) == "" || !preg_replace('/\s/', '', $data->profile)) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The profile value is not correct");
                $check = false;
            }
        }

        if ($check) {
            $pf = preg_replace('/\s/', '', trim(strip_tags($data->profile)));
            $pTemp = $this->em->getRepository(Settings::class)
                ->findOneBy(["typename" =>Constants::SETTINGS_PROFILE, "keyname" => $pf]);

            if ($pTemp != null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This profile already exists");
                $check = false;
            }
        }

        if ($check) {
            $setting = new Settings();
            $setting->setTypename(Constants::SETTINGS_PROFILE);
            $setting->setKeyname($pf);
            $setting->setValue(trim(filter_var($data->nom), FILTER_SANITIZE_STRING));

            if ($this->dataPersister->supports($setting)) {
                $this->dataPersister->persist($setting);

                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "profile" => $setting->getKeyname(),
                    "nom" => $setting->getValue(),
                ]);
                
                $listSecZone = $this->em->getRepository(Settings::class)
                    ->listByType(Constants::SETTINGS_SECZONE, ["keyname"=>"keyname", "value"=>"value"]);
                if (!empty($listSecZone)) {
                    foreach ($listSecZone as $zone){
                        $right = new Rights();
                        $right->setZone($zone['keyname'])
                            ->setProfile($setting->getKeyname())
                            ->setLevel(0);
                        $this->em->persist($right);
                    }

                    $this->em->flush();
                }

            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }

    public function list(ApiResponse $apiResponse)
    {
        $profiles = $this->em->getRepository(Settings::class)
            ->listByType(Constants::SETTINGS_PROFILE, ["keyname"=>"profile", "value"=>"nom"]);

        if (!empty($profiles)) {
            $apiResponse->setData($profiles);
        }

        return $apiResponse;
    }

    public function update(ApiResponse $apiResponse, ProfileDto $data, int $id)
    {
        $check = true;
        if ($check) {
            $profile = $this->em->getRepository(Settings::class)
                ->findOneBy(["typename" =>Constants::SETTINGS_PROFILE, "id" => $id]);
            if ($profile === null) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("This profile does not exist");
                $check = false;
            }
        }

        if ($check) {
            if (strlen(filter_var(trim($data->nom), FILTER_SANITIZE_STRING)) === 0) {
                $apiResponse->setCode(0);
                $apiResponse->setMessage("The nom should be a valid string");
                $check = false;
            }
        }

        if ($check) {
            $profile->setValue(trim(filter_var($data->nom), FILTER_SANITIZE_STRING));

            if ($this->dataPersister->supports($profile)) {
                $this->dataPersister->persist($profile);

                $apiResponse->setContext([
                    "id" => $id
                ]);
                $apiResponse->setMessage(Constants::MSG_SUCCESS_SAVE);
                $apiResponse->setData([
                    "id" => $id,
                    "profile" => $profile->getKeyname(),
                    "nom" => $profile->getValue(),
                ]);
            } else {
                $apiResponse->setCode(0);
                $apiResponse->setMessage(Constants::MSG_ENTITY_NOT_SUPPORTED);
            }
        }

        return $apiResponse;
    }
}