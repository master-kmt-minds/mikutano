<?php


namespace App\Service;


use App\Entity\ApiBaseEntity;
use App\Service\SDK\BaseService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EntityService extends BaseService
{
    public function markAsDeleted(ApiBaseEntity $data, string $className)
    {
        /**
         * @var ApiBaseEntity $entity
         */
        $entity = $this->em->getRepository($className)
            ->findOneBy(["id" => $data->getId()]);
        if ($entity === null || $entity->getDeleted()) {
            throw new NotFoundHttpException('Unable to find this resource.');
        }

        $entity->setDeleted(true);
        if ($this->dataPersister->supports($entity)) {
            $this->dataPersister->persist($entity);
        } else {
            throw new BadRequestHttpException('An error occurs. Please do try later.');
        }
    }

}