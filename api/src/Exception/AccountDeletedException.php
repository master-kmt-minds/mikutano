<?php


namespace App\Exception;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountDeletedException extends AccountStatusException
{
    /**
     * AccountDeletedException constructor.
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
        $this->message = $message;
    }

    public function getMessageKey()
    {
        return $this->message;
    }

}