<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 14/03/19
 * Time: 20:04
 */

namespace App;


final class Constants
{
    /*******************************************************************
     * Default Settings "setting type"
     *******************************************************************/
    public const SETTINGS_PROFILE = 'profile';
    public const SETTINGS_SECZONE = 'securityZone';

    /*******************************************************************
     * Default security zone
     *******************************************************************/
    public const SECZONE_OPEN_ZONE = 'open_zone';
    public const SECZONE_BO_ZONE = 'bo_zone';
    public const SECZONE_USERS_ZONE = 'bo_users_zone';

    /*******************************************************************
     * Default profile
     *******************************************************************/
    public const PROFILE_ANONYMOUS = "anonymous";
    public const PROFILE_GUEST = "guest";
    public const PROFILE_ADMIN = "admin";
    public const PROFILE_SUPERADMIN = "superAdmin";

    /*******************************************************************
     * Default access levels
     *******************************************************************/
    public const SECURITY_NOACCESS = 0;
    public const SECURITY_READONLY = 1;
    public const SECURITY_FULLACCESS = 2;


    /*******************************************************************
     * Regex requirement
     *******************************************************************/
    public const PASSWORD_REGEX = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$/';
    public const CM_PHONE_NUMBER_REGEX = '/^(([+]|00)(237))*[0-9]{9}$/';

    /*******************************************************************
     * Messages
     *******************************************************************/
    public const MSG_NOAUTHORISED = 'Sorry: Access Denied.';
    public const MSG_SUCCESS_SAVE = 'Data saved.';
    public const MSG_ENTITY_NOT_SUPPORTED = 'Entity manager can not handle the persist process of this entity.';
    public const MSG_USER_NOT_CONFIRMED = 'Please confirm first your email address through the link you received';
    public const MSG_USER_NOT_ACTIVED = 'This account has been closed. Contact us for help';
    public const MSG_USER_BEEN_BLOCKED = 'Your account has been blocked. Contact us for more details.';

    /*******************************************************************
     * Default mail messages
     *******************************************************************/
    public const MAIL_DEFAULT_SENDER_MAIL = 'tgduhamel@gmail.com';
    public const MAIL_WELCOME_OBJECT = 'Bienvenue !';
    public const MAIL_RESET_PASSWORD_OBJECT = 'Changez votre mot de passe';
    public const MAIL_UNSUBSCRIBE_OBJECT = 'Au revoir ?!';
    public const MAIL_LOCK_USER_OBJECT = 'Oops! Votre compte a été bloqué';
    public const MAIL_UNLOCK_USER_OBJECT = 'Youpi! You have been unblocked';

    /*******************************************************************
     * Some generic params
     *******************************************************************/
    public const GENERIC_TOKEN_LENGTH = 6;
    public const TOKEN_VALIDITY_REGISTRATION = 1440; //in minutes
    public const TOKEN_VALIDITY_RESET_PASSWORD = 15; //in minutes

    /*******************************************************************
     * Date & Time regex check
     *******************************************************************/
    public const REGEX_DATE = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
    public const REGEX_TIME = "/^([01][0-9]|2[0-3]):[0-5][0-9]$/"; //in minutes

}