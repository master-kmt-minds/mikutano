# Kesako ?
__Simple app made in PHP (API & Back office) and Flutter (Front).__


This starter kit is based on :
- Docker (docker-compose)
- Symfony4.4 for the back and the API
- The API is based on API Platform 
- Flutter is used for the front side

# TIPS (Installation)
### Check .env files
#### API .env files
If you change containers names (in docker-compose.yml file), make sure first to update db 
infos in the API. <br> 
In the api/.env file, you have to change **container-db-name** with the name of 
the db service (as defined at *container_name* property of db service in docker-compose.yml) :
```
21 | DATABASE_URL=postgres://admin:admin@container-db-name/api
```
#### Back .env files
Make sure that those params ask for the right API address **API_URL, API_LOGIN_CHECK, API_REFRESH_TOKEN**

### Install
You can now install :) <br>
run ___docker-compose up___ to install. 
Add the flag __-d__ if you want it to run in background mode.

__You can change services ports in docker-compose.yml file__ <br><br>

__$ docker ps__ show list of containers with their ports
___
### Access to your container
Access to a specific container to run some command you need.

> $ docker exec -ti container_name sh

Replace _container_name_ with your container name (found under the NAMES columns after running *docker ps* ).
____
### Build database and import default data 
* Copy default data (dataexport.pgsql) in DB container. While being in the project root, run : 
> $ docker cp dataexport.pgsql container-db-name:/
* Go to the DB container  (see how to **Access to tyour container**) and import the data in the database : 
> psql -U admin -d api -f dbexport.pgsql

____
*You should be ok now :)*

### Default user credentials ! !
username: **duhamel@gm.com** <br>
password: **Douala@237**


__Notes :__

Some concepts used :
- subresources && subresources operations (API Platform speaking)
- inheritance. Then abandoned because a bit complicated
- Custom GET operations for subresources (see ConferenceParticipantListAction::class)