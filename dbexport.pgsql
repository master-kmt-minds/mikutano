--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.12
-- Dumped by pg_dump version 9.6.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agences; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.agences (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    infos text
);


ALTER TABLE public.agences OWNER TO admin;

--
-- Name: agences_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.agences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agences_id_seq OWNER TO admin;

--
-- Name: conference_participant; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.conference_participant (
    conference_id integer NOT NULL,
    participant_id integer NOT NULL
);


ALTER TABLE public.conference_participant OWNER TO admin;

--
-- Name: conference_participant_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.conference_participant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conference_participant_id_seq OWNER TO admin;

--
-- Name: conference_speaker; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.conference_speaker (
    conference_id integer NOT NULL,
    speaker_id integer NOT NULL
);


ALTER TABLE public.conference_speaker OWNER TO admin;

--
-- Name: conferences; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.conferences (
    id integer NOT NULL,
    created_by integer,
    title character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    date_start date NOT NULL,
    hour_start time(0) without time zone NOT NULL,
    date_end date,
    hour_end time(0) without time zone DEFAULT NULL::time without time zone,
    price numeric(10,2) DEFAULT '0'::numeric NOT NULL
);


ALTER TABLE public.conferences OWNER TO admin;

--
-- Name: conferences_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.conferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conferences_id_seq OWNER TO admin;

--
-- Name: migration_versions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.migration_versions (
    version character varying(14) NOT NULL,
    executed_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.migration_versions OWNER TO admin;

--
-- Name: COLUMN migration_versions.executed_at; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON COLUMN public.migration_versions.executed_at IS '(DC2Type:datetime_immutable)';


--
-- Name: participants; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.participants (
    id integer NOT NULL,
    created_by integer,
    name character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    email character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.participants OWNER TO admin;

--
-- Name: participants_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participants_id_seq OWNER TO admin;

--
-- Name: refresh_tokens; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.refresh_tokens (
    id integer NOT NULL,
    refresh_token character varying(128) NOT NULL,
    username character varying(255) NOT NULL,
    valid timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.refresh_tokens OWNER TO admin;

--
-- Name: refresh_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.refresh_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.refresh_tokens_id_seq OWNER TO admin;

--
-- Name: rights; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.rights (
    id integer NOT NULL,
    profile character varying(255) NOT NULL,
    zone character varying(255) NOT NULL,
    level integer NOT NULL
);


ALTER TABLE public.rights OWNER TO admin;

--
-- Name: rights_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rights_id_seq OWNER TO admin;

--
-- Name: settings; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    typename character varying(255) NOT NULL,
    keyname character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.settings OWNER TO admin;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO admin;

--
-- Name: speakers; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.speakers (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    email character varying(255) DEFAULT NULL::character varying,
    photo character varying(255) DEFAULT NULL::character varying,
    created_by integer
);


ALTER TABLE public.speakers OWNER TO admin;

--
-- Name: speakers_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.speakers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.speakers_id_seq OWNER TO admin;

--
-- Name: users; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.users (
    id integer NOT NULL,
    nom character varying(180) NOT NULL,
    prenom character varying(180) NOT NULL,
    ville character varying(180) NOT NULL,
    telephone character varying(18) NOT NULL,
    email character varying(80) NOT NULL,
    profile character varying(255) NOT NULL,
    active boolean NOT NULL,
    blocked boolean NOT NULL,
    password character varying(255) DEFAULT NULL::character varying,
    temptoken character varying(255) DEFAULT NULL::character varying,
    tokenvaliditydate timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.users OWNER TO admin;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO admin;

--
-- Data for Name: agences; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.agences (id, name, address, phone, infos) FROM stdin;
1	Kokuta	58 Bilouma Rikonu 3211 DANKERE	+0034322424442	Blablabla
\.


--
-- Name: agences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.agences_id_seq', 1, true);


--
-- Data for Name: conference_participant; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.conference_participant (conference_id, participant_id) FROM stdin;
\.


--
-- Name: conference_participant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.conference_participant_id_seq', 1, false);


--
-- Data for Name: conference_speaker; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.conference_speaker (conference_id, speaker_id) FROM stdin;
15	1
\.


--
-- Data for Name: conferences; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.conferences (id, created_by, title, address, date_start, hour_start, date_end, hour_end, price) FROM stdin;
16	1	string	string	2019-11-02	08:30:12	2019-11-02	08:30:12	0.00
17	1	string	string	2019-11-02	08:30:12	2019-11-02	08:30:12	0.00
15	1	Conf edit	18 18	2019-11-02	08:30:12	2019-11-02	08:30:12	15.00
\.


--
-- Name: conferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.conferences_id_seq', 17, true);


--
-- Data for Name: migration_versions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.migration_versions (version, executed_at) FROM stdin;
20191031164615	2019-10-31 16:47:01
20191101110943	2019-11-01 11:09:52
20191101112348	2019-11-01 11:23:58
20191101112441	2019-11-01 11:24:46
20191101113716	2019-11-01 11:37:21
20191101133036	2019-11-01 13:30:42
20191101134028	2019-11-01 13:40:32
20191101134816	2019-11-01 13:48:20
20191102085026	2019-11-02 08:50:29
20191102120746	2019-11-02 12:07:50
20191102124517	2019-11-02 12:45:22
20191102140653	2019-11-02 14:06:57
20191111135714	2019-11-11 13:57:21
20191111135914	2019-11-11 13:59:18
20191117205847	2019-11-17 20:59:07
20191117210704	2019-11-17 21:07:10
20191208190943	2019-12-08 19:10:20
\.


--
-- Data for Name: participants; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.participants (id, created_by, name, phone, email) FROM stdin;
1	1	Duhamel	0758723942	test@test.com
2	1	Gildas	0938438343	test@mail.com
\.


--
-- Name: participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.participants_id_seq', 6, true);


--
-- Data for Name: refresh_tokens; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.refresh_tokens (id, refresh_token, username, valid) FROM stdin;
1	bba2e59fddf7c4969ddb6bc6820c1b8ed718e9d02751a9555add9d5fc8cedf352e8e1734943e7e72ce27bc753b40fcd2defdee918ec3193ab366c086998f89dd	duhamel@gm.com	2019-11-28 17:28:54
2	25ae5ea55b0c2de4ee91c269e52e08afbf0d7c213e3f154491742470a644bffae2c9199fb9dc649894a08c36ab2bc9f4cc4f86dad8f98c7d451fdcf6336ba042	duhamel@gm.com	2019-11-28 18:22:12
3	70a670469d1ed827e1669f3f921c52e4d74d8e9a390d5a003e6159fce000533a0d9c279fbf24509c419cfbf46c6f7d9572b89f6e08e14ec5650b449467607603	duhamel@gm.com	2019-11-28 18:22:21
4	d19ad060e1767b9c829e92e558db6f2504199cc9ee8b5708f912cd5fa59a96de49d81382fb93f7c22affba1df7c738051d7d1cc4fd4c9166d24e378dfc5f8323	duhamel@gm.com	2019-11-28 18:22:48
5	fd8315c838a952a8d2c61e2b6747a3a42b3c345cc4ddb76e39df5d1e0b32f17c3cddc7b6faf898778249a0ea8a5e1366ab5799cff295af8f89841043e72cf0ba	duhamel@gm.com	2019-11-28 18:22:51
6	91f5d3bbfac38767fdb6a7407fefacff0eba74ac0228f0153f6ea2083304cfa3675f6c34e818e1b6ae9ec2b7d207c2f87e7c9a666084f9fc94a74436a7b85a4b	duhamel@gm.com	2019-11-28 18:22:53
7	0d9c91c32cb111317719b645930213de302c7f8c087661a6e6f81360024f420c5778598e17e24d214242b73962175df226c0b515c4712dbf83d66f86c782307a	duhamel@gm.com	2019-11-28 18:23:19
8	e2137e7e0125a6b4d75a1c1e2e045560a4925568b07f490969be069a5605b75f475a8aa8024f12a22d86c1767bf01f071dac5e345363a403d9ad898ff955f421	duhamel@gm.com	2019-11-28 18:43:34
9	8bfc13b79dc2fc2d87c21d2d6b528068ffdbe2f9a1dd5fb4be99d1aef7fc73f594d22e035c05456f8462996a7714555d8957acf0acc41f0510d875fd3a4dd64f	duhamel@gm.com	2019-11-28 18:45:17
10	112784ca500e4a7f462d74a5202144f4613a22f83296e9c8524393096c0eb9bf02bba056f6aa9040112d0c7c88e1ad036cc4a528bfed12a53a30522bf7271d0f	duhamel@gm.com	2019-11-30 14:51:39
11	48bd808011f9911c2f8a5f7d71d3a9fa933717590f84011942cf404c61431427fba93a76741738a3e71d1bb37fed28b84ca16177ef0161e12d89f2ee1e2beef9	duhamel@gm.com	2019-12-01 15:04:59
12	b0ef3939e01fc2fe8fc5804250bf5731428d8c09b7ad0d432c48e2140255cb386845b736e4f0114dd66fc0fc4df82815d2cf5653502a91858194409b274c5115	duhamel@gm.com	2019-12-01 16:44:19
13	78314f6633eacde538012bf4ef4219e62c69c949db2a4e37cb0321094d706f75360dee72876a951724b7d03ef641e82f6ee32300f1c2bcf38902379e154629d1	duhamel@gm.com	2019-12-01 16:45:55
14	6dcbdc6b6101fc196f57ceafd22efef635943f2d3fcf253c67553c68db9806c97dc5151fa73b32eab268785cef9585aae681eed458377fd7cec224a9b30ed029	duhamel@gm.com	2019-12-01 17:04:08
15	3faa8e1c748336d1813947ada2de6b42c22c0ca93ee3d0ef64b1c36f97ac3e12c96b2dbd10e2b5989e1818be9066b85f825085890c6150f98d738b3ae9986103	duhamel@gm.com	2019-12-01 17:13:35
16	5ac4d36dd45cf9505f07a9dbedb39ffd431217590cc09ee42efcff5472329f847945e20d7df79c6f99fe0d71ebcad79c007cc9dec6902101cd4b07f9755e23df	duhamel@gm.com	2019-12-01 20:08:16
17	2e47db6d88142ac0ae0eb27f613c823ee3d1d0d2e2970c33d24e5705d536cc17620835e278085eb023203ff5e48a1060145f0230ddf709ff1f97d17f6b37d24e	duhamel@gm.com	2019-12-01 23:54:36
18	0fa007a36c5c26a10c2d16aee2910cbbaa154ec037e1ad7073fef21bad0783127cf73d2978ba4ee99855c04c530603c9e77939f4515f5fddf0e373e835d3af28	duhamel@gm.com	2019-12-02 10:01:05
19	03d567f839ebbff8ce37aebf39d7bd82b512501fccbb399da0912eb84555eba1f344f2d00a0ec6c1e09367dc80d19f8c460fc12868623e87d2a29fc8ba5649e7	duhamel@gm.com	2019-12-02 14:16:00
20	862931320e306d38193060b7e144011c38055b0a4413332201559343c0898ffb911e80ca710914e72cc2c10ce4d4686b6c69d230e5d3b679de07154b95aeb6b5	duhamel@gm.com	2019-12-02 14:44:47
21	bdb31e3650bfb0366e5bb8349e07732765d89240e67b763013d9a9731834fd0f5f243b4a7387be4b4421a1e6826690e9241b4dd1afeb6918aa1cc6ddb0caf289	duhamel@gm.com	2019-12-02 15:04:54
22	d5a47df8cf7513a594057ee510ea22adeec8ff523b71ac5096181933941ec84837c0efc4718d75daece74e096fec6659ab065dfc8f11de1d88982ddd882e7746	duhamel@gm.com	2019-12-02 16:07:09
23	a869083d218e59337c52c923180524eef912f27fb038fd7c325b0b9ef7e8c1c08ad6f8aa0d3da1414f0aeb21e1f763a6fd4f39df3886fac5e3e266e497344cdf	duhamel@gm.com	2019-12-02 16:23:53
24	09882a2dc17efedac1a510d3cbb25611ad718dbafe035862c050021bf9b75a03549358fc8917a433ccf04ea1f625fd2f7cb39095dc344a04e1b1873f66b287e3	duhamel@gm.com	2019-12-02 16:34:46
25	6c75b6a4ee4774a69c5d80d4f4a3dd21ca76bfb5d0ec0e77b76cee549cf73a314609d3c7df47ec40bea5ba3655821f9ac73e298afc755a841fd5658dbb60a3c6	duhamel@gm.com	2019-12-02 17:18:50
26	70c63ba397ea2baaf697271dc053c9ec48d50edd4071c774c3cbd6096e8abc948371e5ee48b932aca404d4984336ac107fcfe16fbf2932a5e5d6b069896cee88	duhamel@gm.com	2019-12-02 19:32:14
27	6a048b6b9ca27452d61389c860756d91b6fda318d797a0b6908d7c2a0f08160c9546ea78df2ae196a6f81d72c97b1480313939e94a9244aafa6f845dccc53221	duhamel@gm.com	2019-12-02 19:49:29
28	5a4d3d3e05e3b455953d6069102ac868c6fabfed844fa983d38d1fc8f00f85ca40db40368ab52d1d80a967cc760ba5013788cb5daaf69e10cef30e338346e22c	duhamel@gm.com	2019-12-17 21:58:11
29	ccc5b340f62d03d5b6f6d68e771623f3ee13b08b6d90aa8e9bff64930c948fec5e341a7a2748e0afb56db8a3e6b968ba8c425d2f09e158892ba16fb751181b9c	duhamel@gm.com	2020-01-15 22:17:18
\.


--
-- Name: refresh_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.refresh_tokens_id_seq', 29, true);


--
-- Data for Name: rights; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.rights (id, profile, zone, level) FROM stdin;
1	superAdmin	open_zone	2
2	superAdmin	bo_zone	2
3	superAdmin	bo_users_zone	2
\.


--
-- Name: rights_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.rights_id_seq', 1, false);


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.settings (id, typename, keyname, value) FROM stdin;
1	profile	superAdmin	Super Administrateur
2	securityZone	open_zone	Zone neutre
3	securityZone	bo_zone	Back Office
4	securityZone	bo_users_zone	Gestion des users
\.


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.settings_id_seq', 1, false);


--
-- Data for Name: speakers; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.speakers (id, name, phone, email, photo, created_by) FROM stdin;
1	Speaker	0758913942	mail@mail.com	string	1
2	Speaker 1	0758913942	mail@mail.com	string	1
3	Speaker 1	0758913942	mail@mail.com	string	1
\.


--
-- Name: speakers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.speakers_id_seq', 3, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.users (id, nom, prenom, ville, telephone, email, profile, active, blocked, password, temptoken, tokenvaliditydate) FROM stdin;
1	Tchafack	Duhamel	D.	694030591	duhamel@gm.com	superAdmin	t	f	$argon2i$v=19$m=1024,t=2,p=2$QWE0RDhtM1Nqb3BSVkFhNQ$JiFR8+xp5Gd76Lc03nKTwhfawqQJcPO7RNBchwJUpAs	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: agences agences_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.agences
    ADD CONSTRAINT agences_pkey PRIMARY KEY (id);


--
-- Name: conference_participant conference_participant_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_participant
    ADD CONSTRAINT conference_participant_pkey PRIMARY KEY (conference_id, participant_id);


--
-- Name: conference_speaker conference_speaker_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_speaker
    ADD CONSTRAINT conference_speaker_pkey PRIMARY KEY (conference_id, speaker_id);


--
-- Name: conferences conferences_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conferences
    ADD CONSTRAINT conferences_pkey PRIMARY KEY (id);


--
-- Name: migration_versions migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.migration_versions
    ADD CONSTRAINT migration_versions_pkey PRIMARY KEY (version);


--
-- Name: participants participants_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participants_pkey PRIMARY KEY (id);


--
-- Name: refresh_tokens refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.refresh_tokens
    ADD CONSTRAINT refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: rights rights_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.rights
    ADD CONSTRAINT rights_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: speakers speakers_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.speakers
    ADD CONSTRAINT speakers_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: idx_21c01b1ede12ab56; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_21c01b1ede12ab56 ON public.speakers USING btree (created_by);


--
-- Name: idx_71697092de12ab56; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_71697092de12ab56 ON public.participants USING btree (created_by);


--
-- Name: idx_726b46cd604b8382; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_726b46cd604b8382 ON public.conference_participant USING btree (conference_id);


--
-- Name: idx_726b46cd9d1c3019; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_726b46cd9d1c3019 ON public.conference_participant USING btree (participant_id);


--
-- Name: idx_807844d9604b8382; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_807844d9604b8382 ON public.conference_speaker USING btree (conference_id);


--
-- Name: idx_807844d9d04a0f27; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_807844d9d04a0f27 ON public.conference_speaker USING btree (speaker_id);


--
-- Name: idx_8e2090bade12ab56; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX idx_8e2090bade12ab56 ON public.conferences USING btree (created_by);


--
-- Name: rgt_un; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX rgt_un ON public.rights USING btree (profile, zone);


--
-- Name: uniq_1483a5e9e7927c74; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX uniq_1483a5e9e7927c74 ON public.users USING btree (email);


--
-- Name: uniq_9bace7e1c74f2195; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX uniq_9bace7e1c74f2195 ON public.refresh_tokens USING btree (refresh_token);


--
-- Name: users_uniq; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX users_uniq ON public.users USING btree (email, password);


--
-- Name: speakers fk_21c01b1ede12ab56; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.speakers
    ADD CONSTRAINT fk_21c01b1ede12ab56 FOREIGN KEY (created_by) REFERENCES public.users(id);


--
-- Name: participants fk_71697092de12ab56; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.participants
    ADD CONSTRAINT fk_71697092de12ab56 FOREIGN KEY (created_by) REFERENCES public.users(id);


--
-- Name: conference_participant fk_726b46cd604b8382; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_participant
    ADD CONSTRAINT fk_726b46cd604b8382 FOREIGN KEY (conference_id) REFERENCES public.conferences(id) ON DELETE CASCADE;


--
-- Name: conference_participant fk_726b46cd9d1c3019; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_participant
    ADD CONSTRAINT fk_726b46cd9d1c3019 FOREIGN KEY (participant_id) REFERENCES public.participants(id) ON DELETE CASCADE;


--
-- Name: conference_speaker fk_807844d9604b8382; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_speaker
    ADD CONSTRAINT fk_807844d9604b8382 FOREIGN KEY (conference_id) REFERENCES public.conferences(id) ON DELETE CASCADE;


--
-- Name: conference_speaker fk_807844d9d04a0f27; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conference_speaker
    ADD CONSTRAINT fk_807844d9d04a0f27 FOREIGN KEY (speaker_id) REFERENCES public.speakers(id) ON DELETE CASCADE;


--
-- Name: conferences fk_8e2090bade12ab56; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.conferences
    ADD CONSTRAINT fk_8e2090bade12ab56 FOREIGN KEY (created_by) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

