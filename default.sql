--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.12
-- Dumped by pg_dump version 11.2

-- Started on 2019-08-03 23:12:21 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 219 (class 1259 OID 16675)
-- Name: users; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying(80) NOT NULL,
    password character varying(255),
    nom character varying(180) NOT NULL,
    prenom character varying(180) NOT NULL,
    ville character varying(180) NOT NULL,
    telephone character varying(18) NOT NULL,
    active boolean NOT NULL,
    blocked boolean NOT NULL,
    profile character varying(255) NOT NULL,
    temptoken character varying(255) DEFAULT NULL::character varying,
    tokenvaliditydate timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.users OWNER TO admin;

--
-- TOC entry 2229 (class 0 OID 16675)
-- Dependencies: 219
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.users (id, email, password, nom, prenom, ville, telephone, active, blocked, profile, temptoken, tokenvaliditydate) FROM stdin;
1	duhamel@gm.com	$argon2i$v=19$m=1024,t=2,p=2$QWE0RDhtM1Nqb3BSVkFhNQ$JiFR8+xp5Gd76Lc03nKTwhfawqQJcPO7RNBchwJUpAs	Tchafack	S.Admin	string	694030591	t	f	superAdmin	\N	\N
2	tgduhamel@yahoo.fr	$argon2i$v=19$m=1024,t=2,p=2$dnJHOGh1cVouZDhMUXo0WQ$teUPD23P+79UAO6Yg7Mk0LKDAifJWridxXQBmVtLNog	tchafack	duhamel	Mfou	694030591	f	fanonymous	a59040b03c02	2019-05-21 16:38:57
\.


--
-- TOC entry 2110 (class 2606 OID 16682)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2108 (class 1259 OID 16720)
-- Name: uniq_1483a5e9e7927c74; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX uniq_1483a5e9e7927c74 ON public.users USING btree (email);


--
-- TOC entry 2111 (class 1259 OID 41448)
-- Name: users_uniq; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX users_uniq ON public.users USING btree (email, password);


-- Completed on 2019-08-03 23:12:21 UTC

--
-- PostgreSQL database dump complete
--
